# **VARIPHYER**

**VariPhyer** is a benchmark evaluation pipeline which is implemented with nextflow, Groovy, Python, and other 19 common 
genomic analysis applications. It is a comprehensive workflow for verifying phylogenetic analyses and promoting phylogenomic best practices

## Step 1: Install nextflow and Singularity (If you already have Nextflow and Singularity installed on your system, you can skip this step.)
Currently, VariPhyer can only be installed on a Linux system. Either on a Linux server, or the Windows Linux subsystem. Currently, VariPhyer doesn't support Mac.
Before install this workflow, make sure **Java** 1.8.0_221 is installed
```
java -version
```
[Install](https://www.nextflow.io/docs/latest/getstarted.html#installation) **Nextflow** v19.07.0.5106 \
[Install](https://sylabs.io/guides/3.6/user-guide/quick_start.html#quick-installation-steps) **Singularity** v3.6

## Step 2: Download the pipeline and set paths
```
git clone git@gitlab.com:treangenlab/variphyer.git
cd variphyer/
```

The `nextflow.config` file includes a `program` parameter that needs to be set to the absolute path of the `phylo_programs` directory as well as default parameters for the input and outputs. 

## Step 3: Getting Started

## Usage:

     The typical command for running the pipeline is as follows:
     
        nextflow run main.nf [read type] [mode] [optional path]

     Mandatory arguments:
       --s, --r or --a               running mode, --s is simulate from genome, --r is to start from reads, --a is to start from alignment from either contigs or genomes
       --longMode, or --shortMode            reads type, --longMode is to run long reads workflow, --shortMode is to run short reads workflow

     Options:
        --ref                        Genomes used for simulation
        --oneRef                     Reference genome dir, default is "refSeq/GCF_000240185.1_ASM24018v2_genomic.fna"
        --outputDir                  The output directory where the results will be saved
        --program                    Script full path, for example [your-full-path]/phylo_programs/
        --shortPath                  Directory of illumina pair-end reads, the format could be for example: "[path-to-files]/*/*{_1,2}.f*"
        --longPath                   Directory of long reads, for example: "[path-to-files]/*/*.fast*"
        --longAlignmentPath          Directory of long contigs
        --shortAlignmentPath         Directory of short contigs
        --test                       To test the all the unit tests to make sure the installation is succeed

     Other options:
       --help                        Show this message and exit.

     Softwares and dependencies:
         MEGAHIT v1.2.3-beta
         Canu v2.0
         parsnp v1.5
         mauve_linux_snapshot_2015-02-13
         stripSubsetLCBs
         RAxML
         SPAdes-3.13.1-Linux
         harvesttools-Linux64-v1.2
         artsrcmountrainier2016.06.05linux
         gsl-2.5
         PBSIM-PacBio-Simulator v1.0.4
         biopython
         BEASTv1.10.4
         BEASTGen_v1.0.2
         beagle-lib
         Trimmomatic-0.39
         mash v1.1
         FastTree v2.1.10
         numpy
         fastani
         pybedtools
         scipy
         scikit-learn
         htseq
         lofreq v2.1.3.1
         minimap2v2.17-r941
         bowtie2-align-s v2.3.5.1
         ngmlr v0.2.7
         samtools v1.3.1
         sniffles v1.0.12
         breseq v0.35.2
         MAFFT v7.471

All the parameters can be set in the config file.

## Containers
Phyer.sif, VCContainer.sif, and lofreq.sif containers are included for associated processes. The container usage is in config file.
Run the following commands to download each containers from [Singularity Library](https://cloud.sylabs.io/library/):


```
mkdir container
cd container

singularity pull library://cl117/default/phyer.sif:sha256.faa3ced46c852f5ccf724ce833504a971adb2380375edbe3bc438dc20bfc51b8
mv variphyer* phyer.sif

singularity pull library://cl117/default/vc1:sha256.723e40e937af5f03fb235343aceb003f8cf6dc5b07107fabedb9b196710bb53a
mv vc1* VCContainer.sif

singularity pull library://cl117/default/vc1:sha256.990ba7f501eeca0b2402f074e9c8e67f970e2293bdccb403614355dbd5329370
mv lofreq1* lofreq.sif

singularity pull library://cl117/default/singularity-r:sha256.dd17636dffab90751208e235cf425544ee5f71f3a860b6fff91a69e0d499abbd
mv singularity-r* singularity-r.sif
cd ..

```

## Testing command
To test the pipeline with given examples, run the following command. The downloaded folder include a test data set, all the
parameters to run this data set are in the nextflow.config file. If you want to run a different data set, those parameters
need to be changed to new data set. 

```
nextflow run main.nf -resume --longMode --shortMode --s -with-dag flowcharta.png -with-timeline timelinea.html
```

## Output
If the pipeline is successfully run, you should see each process is 100% complete.

```
(base) cl117@gho:~/nextflowWork$ nextflow run main.nf -resume --longMode --shortMode --s -with-dag flowcharta.png -with-timeline timelinea.html
N E X T F L O W  ~  version 19.07.0
Launching `main.nf` [berserk_golick] - revision: 7408ed6850
[2d/6d8108] process > pbSim_HiFi (genome_1)               [100%] 4 of 4, cached: 4 ✔
[fe/150a44] process > pbSim (genome_4)                    [100%] 4 of 4, cached: 4 ✔
[db/98d320] process > art_illumina_process (genome_1)     [100%] 4 of 4, cached: 4 ✔
[e0/c7b483] process > trim_illumina (genome_1)            [100%] 4 of 4, cached: 4 ✔
[45/61e61d] process > minimap2_illumina (genome_3)        [100%] 4 of 4, cached: 4 ✔
[d6/bd8054] process > minimap2_pacbio_CLR (genome_3)      [100%] 4 of 4, cached: 4 ✔
[50/99f8fd] process > minimap2_pacbio_CCS (genome_3)      [100%] 4 of 4, cached: 4 ✔
[61/e53405] process > bowtie_index                        [100%] 1 of 1, cached: 1 ✔
[39/11def2] process > bowtie_illumina (genome_2)          [100%] 4 of 4, cached: 4 ✔
[f3/68e3f2] process > NGMLR (genome_1)                    [100%] 4 of 4, cached: 4 ✔
[16/b7ca06] process > sniffles_ngmlr_pacbio (genome_1)    [100%] 4 of 4, cached: 4 ✔
[de/98f0a6] process > breseq_illumina (genome_2)          [100%] 4 of 4 ✔
[fc/5dddd7] process > lofreq_illumina_minimap (genome_3)  [100%] 4 of 4, cached: 4 ✔
[e3/d9121f] process > lofreq_pacbioCCS_minimap (genome_2) [100%] 4 of 4, cached: 4 ✔
[30/9ec8cd] process > lofreq_illumina_bowtie (genome_1)   [100%] 4 of 4, cached: 4 ✔
[f2/fdedad] process > lofreq_NGMLR (genome_1)             [100%] 4 of 4, cached: 4 ✔
[80/a74206] process > canu_pbSim (genome_3)               [100%] 4 of 4 ✔
[2e/ae43e2] process > merge_canu_pacbio                   [100%] 1 of 1 ✔
[77/840bbd] process > parsnp_pacbio (1)                   [100%] 1 of 1 ✔
[76/f4d728] process > Raxml_parsnp_pacbio (1)             [100%] 1 of 1 ✔
[0b/49908e] process > Beast_parsnp_pacbio (1)             [100%] 1 of 1 ✔
[60/5f1a4b] process > mauve_pacbio (1)                    [100%] 1 of 1 ✔
[20/dfa3ef] process > Raxml_mauve_pacbio (1)              [100%] 1 of 1 ✔
[10/724e81] process > Beast_mauve_pacbio (1)              [100%] 1 of 1 ✔
[88/1bb355] process > megahit_illumina (genome_2)         [100%] 4 of 4, cached: 4 ✔
[e0/eb9f98] process > merge_megahit                       [100%] 1 of 1, cached: 1 ✔
[83/a02f74] process > Parsnp_megahit (1)                  [100%] 1 of 1 ✔
[16/e0702e] process > RAxML_parsnp_megahit (1)            [100%] 1 of 1 ✔
[ab/8e8d4f] process > Beast_parsnp_megahit (1)            [100%] 1 of 1 ✔
[f8/4e221f] process > spades (genome_2)                   [100%] 4 of 4, cached: 4 ✔
[b2/31e5aa] process > merge_spades                        [100%] 1 of 1, cached: 1 ✔
[63/13309a] process > Mauve_spades (1)                    [100%] 1 of 1 ✔
[51/013dfb] process > RAxML_mauve_spades (1)              [100%] 1 of 1 ✔
[1b/a811fe] process > Beast_mauve_spades (1)              [100%] 1 of 1 ✔
[17/b00b56] process > Parsnp_spades (1)                   [100%] 1 of 1, cached: 1 ✔
[6f/5244df] process > RAxML_parsnp_spades (1)             [100%] 1 of 1, cached: 1 ✔
[c3/fbca4e] process > Beast_parsnp_spades (1)             [100%] 1 of 1 ✔
[f4/67e9db] process > spades_hybrid (genome_2)            [100%] 4 of 4, cached: 1 ✔
[86/aaa38c] process > merge_spadesHybrid                  [100%] 1 of 1 ✔
[3d/2f6b4e] process > Mauve_spadesHybrid (1)              [100%] 1 of 1 ✔
[bb/ce312e] process > RAxML_mauve_spadesHybrid (1)        [100%] 1 of 1 ✔
[5a/ba5218] process > Beast_mauve_spadesHybrid (1)        [100%] 1 of 1 ✔
[42/c5c71c] process > Parsnp_spadesHybrid (1)             [100%] 1 of 1 ✔
[06/a7410f] process > RAxML_parsnp_spadesHybrid (1)       [100%] 1 of 1 ✔
[29/b5a0b2] process > Beast_parsnp_spadesHybrid (1)       [100%] 1 of 1 ✔
Completed at: 16-Aug-2020 01:06:12
Duration    : 41m 25s
CPU hours   : 3.9 (35.2% cached)
Succeeded   : 32
Cached      : 70

```

The output is copied to the user defined folder. In this folder, you will see the following folders. Each folder contains the
associated results to each process.

| Folders                           |
| --------------------------------- |
| art_illumina                      |
| art_megahit_assembly              |
| art_padbio_spades_hybrid_assembly |
| art_spades_assembly               |
| bamIllumina                       |
| bamMinimapPacbioCCS               |
| bamNGMLR                          |
| beast_mauve_canu_pacbio           |
| beast_mauve_spades                |
| beast_mauve_spadesHybrid          |
| beast_parsnp_canu_pacbio          |
| beast_parsnp_megahit              |
| beast_parsnp_spades               |
| beast_parsnp_spadesHybrid         |
| breseqIllumina                    |
| canu                              |
| lofreqIllumina                    |
| lofreqIlluminaBowtie              |
| lofreqIlluminaMinimap             |
| lofreqNGMLR_CLR                   |
| lofreqNGMLR_CCS |
| lofreqPacbioCLRMimimap            |
| lofreqPacbioCCSMimimap |
| mauve_canu_pacbio |
| mauve_megahit |
| mauve_spades |
| mauve_spadesHybrid |
| NGMLR_CCS |
| NGMLR_CLR |
| parsnp_canu_pacbio |
| parsnp_megahit |
| parsnp_spades |
| parsnp_spadesHybrid |
| pbSim_HiFi |
| pbSim_normal |
| raxml_mauve_canu_pacbio |
| raxml_mauve_spades |
| raxml_mauve_spadesHybrid |
| raxml_parsnp_canu_pacbio |
| raxml_parsnp_megahit |
| raxml_parsnp_spades |
| raxml_parsnp_spadesHybrid |
| sniffles_ngmlr_CCS |
| sniffles_ngmlr_CLR |
| trim_illumina |

## Other command examples
```
#single
nextflow run main.nf --shortMode --a 
nextflow run main.nf --shortMode --s 
nextflow run main.nf --shortMode --r 
nextflow run main.nf --longMode --s 
nextflow run main.nf --longMode --r 
nextflow run main.nf --longMode --a 
nextflow run main.nf --shortMode --a 

#double
nextflow run main.nf --longMode --shortMode --s 
nextflow run main.nf --longMode --shortMode --r 
nextflow run main.nf --longMode --shortMode --a 
```

To resume the previous cached pipeline:
```
nextflow run main.nf -resume [parameters]
```
To generate workflow image and timeline.html
```
nextflow run main.nf -resume -with-dag flowchart.png -with-timeline timeline.html [parameters]
#For example:
nextflow run main.nf -with-dag flowchart.png -with-timeline timeline.html --longMode --shortMode --s
```

## Pipeline Flowchart
Flowchart of simulation mode:

![Image of Flowchart of running from simulation](images/flowcharta.png)

## Timeline
Timeline and memory usage of each process

| program | memory | time |
| ------- | ------- | ----- |
| art | 20.7 MB | 17.4 s | 
| pbSim | 13.3 MB | 19.5s |
| bowtie_index | 114.8 MB | 4.3s |
| bowtie_illumina | 41.9 MB | 4m 42s |
| breseq_illumina | 670.5 MB | 7m 17s  |
| trim_illumina | 41 MB | 1m 4s |
| NGMLR_CLR | 2.1 GB | 4m 9s |
| NGMLR_CCS | 1.9 GB | 3m 9s |
| sniffles_ngmlr_pacbio_CLR | 21.1 MB | 8.7s |
| sniffles_ngmlr_pacbio_CCS | 9.8 MB | 6s |
| minimap2_pacbio_CCS | 197.8 MB | 34.7s |
| minimap2_pacbio_CLR |  429.6 MB | 1m 19s |
| minimap2_illumina | 3 MB | 1m 4s |
| lofreq_illumina_minimap | 3 MB | 3.5s |
| lofreq_pacbio_CCS_minimap | 549.9 MB | 2h 27m 39s |
| lofreq_pacbio_CLR_minimap | 5.4 GB | 1h 29m 14s  |
| lofreq_NGMLR_CCS | 550.1 MB | 2h 30m 35s |
| lofreq_NGMLR_CLR |  5.4 GB | 2h 6m 30s |
| megahit_illumina | 318.8 MB | 2m 12s |
| spades | 723.6 MB | 2m 7s |
| Mauve_spades | 610.8 MB | 9m 49s |
| mauve_pacbio | 609.2 MB | 5m 46s |
| Parsnp_spades | 1 GB | 1m 17s |
| Parsnp_megahit | 1 GB | 1m 16s |
| Parsnp_spadesHybrid  | 1 GB | 1m 11s |
| parsnp_pacbio | 1 GB | 55.1s |
| RAxML_parsnp_spadesHybrid | 138.9 MB | 11.8s |
| RAxML_mauve_spades | 143 MB | 10.5s |
| RAxML_mauve_spadesHybrid | 165.9 MB | 6.8s |
| Raxml_parsnp_pacbio | 161.9 MB | 6.9s |
| Raxml_mauve_pacbio | 145.6 MB | 6.5s |
| Beast_parsnp_spadesHybrid | 1.1 GB | 3m 1s |
| Beast_mauve_spadesHybrid | 1.1 GB | 2m 30s |
| Beast_parsnp_pacbio | 1 GB | 2m 19s |
| Beast_mauve_pacbio | 1 GB | 2m 24s |
| Beast_parsnp_spades | 831.6 MB | 3m 22s |
| Beast_parsnp_megahit | 1 GB | 3m 43s |
| canu_pbSim | 6.1 GB | 18m 21s |
