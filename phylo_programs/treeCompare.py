import dendropy
from dendropy.calculate import treecompare
import numpy as np
import pandas as pd

tns = dendropy.TaxonNamespace()

treeOriginal = dendropy.datamodel.treemodel.Tree.get_from_path(
    "originalB.tree",
    "newick",
    taxon_namespace=tns)
treeRaxmlMauveCanuPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_mauve_canu_pacbio/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)
treeRaxmlMauveSpades = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_mauve_spades/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)
treeRaxmlMauveSpadesHybrid = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_mauve_spadesHybrid/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)
treeRaxmlParsnpCanuPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_parsnp_canu_pacbio/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)
treeRaxmlParsnpMegahit = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_parsnp_megahit/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)
treeRaxmlParsnpSpades = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_parsnp_spades/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)
treeRaxmlParsnpSpadesHybrid = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/raxml_parsnp_spadesHybrid/RAxML_bestTree._raxml",
    "newick",
    taxon_namespace=tns)

treeOriginal.encode_bipartitions()

treeRaxmlMauveCanuPacbio.encode_bipartitions()
treeRaxmlMauveSpades.encode_bipartitions()
treeRaxmlMauveSpadesHybrid.encode_bipartitions()
treeRaxmlParsnpCanuPacbio.encode_bipartitions()
treeRaxmlParsnpMegahit.encode_bipartitions()
treeRaxmlParsnpSpades.encode_bipartitions()
treeRaxmlParsnpSpadesHybrid.encode_bipartitions()

# dictionary
RtreeDict = {}
RtreeDict["RMauveCanu"] = treeRaxmlMauveCanuPacbio
RtreeDict["RMauveSpades"] = treeRaxmlMauveSpades
RtreeDict["RMauveSpadesHybrid"] = treeRaxmlMauveSpadesHybrid
RtreeDict["RParsnpCanu"] = treeRaxmlParsnpCanuPacbio
RtreeDict["RParsnpMegahit"] = treeRaxmlParsnpMegahit
RtreeDict["RParsnpSpades"] = treeRaxmlParsnpSpades
RtreeDict["RParsnpSpadesHybrid"] = treeRaxmlParsnpSpadesHybrid
# compare every Raxml tree with the original tree
missing = []
treeSize = len(RtreeDict.keys())
for k, tree in RtreeDict.items():
    print(k, 'euclidean_distance', treecompare.euclidean_distance(treeOriginal, tree, is_bipartitions_updated=True))
    print(k, 'false_positives_and_negatives', treecompare.false_positives_and_negatives(treeOriginal, tree, is_bipartitions_updated=True))
    print(k, 'unweighted Robinson-Foulds', treecompare.symmetric_difference(treeOriginal, tree, is_bipartitions_updated=True))
    missing = treecompare.find_missing_bipartitions(treeOriginal, tree, is_bipartitions_updated=True)
    if(len(missing)>0):
        for x in missing:
            print(x);
    #print(k, 'mason_gamer_kellogg_score', treecompare.mason_gamer_kellogg_score(treeOriginal, tree, is_bipartitions_updated=True))
# Compare Raxml with itself with euclidean distance
REtreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'],
                        index=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'])
for k, tree in RtreeDict.items():
        for k2, tree2 in RtreeDict.items():
            REtreePd.loc[k][k2] =treecompare.euclidean_distance(tree, tree2, is_bipartitions_updated=False)
REtreePd.to_csv("table_out_euclidean_distance.csv")


# Compare Raxml with itself with false_positives
RFPTreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'],
                        index=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'])
for k, tree in RtreeDict.items():
        for k2, tree2 in RtreeDict.items():
            RFPTreePd.loc[k][k2] =treecompare.false_positives_and_negatives(tree, tree2, is_bipartitions_updated=False)[0]
RFPTreePd.to_csv("table_out_false_positives.csv")

# Compare Raxml with itself with false_negatives
RFNTreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'],
                        index=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'])
for k, tree in RtreeDict.items():
    for k2, tree2 in RtreeDict.items():
        RFNTreePd.loc[k][k2] =treecompare.false_positives_and_negatives(tree, tree2, is_bipartitions_updated=False)[1]
RFPTreePd.to_csv("table_out_false_negatives.csv")

# Compare Raxml with itself with false_negatives
RRFTreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'],
                        index=['RMauveCanu', 'RMauveSpades', 'RMauveSpadesHybrid', 'RParsnpCanu','RParsnpMegahit','RParsnpSpades', 'RParsnpSpadesHybrid'])
for k, tree in RtreeDict.items():
    for k2, tree2 in RtreeDict.items():
        RRFTreePd.loc[k][k2] =treecompare.symmetric_difference(tree, tree2, is_bipartitions_updated=False)
RRFTreePd.to_csv("table_out_Robinso_Foulds.csv")
