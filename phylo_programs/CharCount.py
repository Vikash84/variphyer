import sys

with open(sys.argv[1], encoding="utf8", errors='ignore') as f:
    j = 0
    for i, line in enumerate(f):
        if (line.startswith('>')):
            print(line.replace('\n', ''))
        else:
            print(len(line.replace('\n','')))
