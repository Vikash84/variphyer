from Bio.Phylo import NewickIO, NexusIO
import sys

input_handle = open(sys.argv[1], "r")
output_handle = open(sys.argv[2], "w")

newick = NewickIO.parse(input_handle)
NexusIO.write(newick, output_handle)

output_handle.close()
input_handle.close()