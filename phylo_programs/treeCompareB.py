import dendropy
from dendropy.calculate import treecompare
import numpy as np
import pandas as pd

tns = dendropy.TaxonNamespace()
treeOriginalB = dendropy.datamodel.treemodel.Tree.get_from_path(
    "originalB.nexus",
    "nexus",
    taxon_namespace=tns)
treeBeastMauveCanuPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_mauve_canu_pacbio/out.txt",
    "nexus",
    taxon_namespace=tns)
treeBeastMauveSpades = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_mauve_spades/out.txt",
    "nexus",
    taxon_namespace=tns)
treeBeastMauveSpadesHybrid = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_mauve_spadesHybrid/out.txt",
    "nexus",
    taxon_namespace=tns)
treeBeastParsnpCanuPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_parsnp_canu_pacbio/out.txt",
    "nexus",
    taxon_namespace=tns)
treeBeastParsnpMegahit = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_parsnp_megahit/out.txt",
    "nexus",
    taxon_namespace=tns)
treeBeastParsnpSpades = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_parsnp_spades/out.txt",
    "nexus",
    taxon_namespace=tns)
treeBeastParsnpSpadesHybrid = dendropy.datamodel.treemodel.Tree.get_from_path(
    "trees_7_26/beast_parsnp_spadesHybrid/out.txt",
    "nexus",
    taxon_namespace=tns)

treeOriginalB.encode_bipartitions()
treeBeastMauveCanuPacbio.encode_bipartitions()
treeBeastMauveSpades.encode_bipartitions()
treeBeastMauveSpadesHybrid.encode_bipartitions()
treeBeastParsnpCanuPacbio.encode_bipartitions()
treeBeastParsnpMegahit.encode_bipartitions()
treeBeastParsnpSpades.encode_bipartitions()
treeBeastParsnpSpadesHybrid.encode_bipartitions()

BTreeDict = {}
BTreeDict["BMauveCanu"] = treeBeastMauveCanuPacbio
BTreeDict["BMauveSpades"] = treeBeastMauveSpades
BTreeDict["BMauveSpadesHybrid"] = treeBeastMauveSpadesHybrid
BTreeDict["BParsnpCanu"] = treeBeastParsnpCanuPacbio
BTreeDict["BParsnpMegahit"] = treeBeastParsnpMegahit
BTreeDict["BParsnpSpades"] = treeBeastParsnpSpades
BTreeDict["BParsnpSpadesHybrid"] = treeBeastParsnpSpadesHybrid

missing = []
treeSize = len(BTreeDict.keys())
# Compare every beast tree with the original tree
for k, tree in BTreeDict.items():
    print(k, 'euclidean_distance', treecompare.euclidean_distance(treeOriginalB, tree, is_bipartitions_updated=False))
    print(k, 'false_positives_and_negatives', treecompare.false_positives_and_negatives(treeOriginalB, tree, is_bipartitions_updated=False))
    print(k, 'unweighted Robinson-Foulds', treecompare.symmetric_difference(treeOriginalB, tree, is_bipartitions_updated=False))
    missing = treecompare.find_missing_bipartitions(treeOriginalB, tree, is_bipartitions_updated=True)
    if(len(missing)>0):
        for x in missing:
            print(x);
# Compare Beast with itself with euclidean distance
BEtreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu', 'BParsnpMegahit','BParsnpSpades', 'BParsnpSpadesHybrid'],
                        index=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu','BParsnpMegahit', 'BParsnpSpades', 'BParsnpSpadesHybrid'])
print(BEtreePd)
for k, tree in BTreeDict.items():
        for k2, tree2 in BTreeDict.items():
            print(k, k2)
            BEtreePd.loc[k][k2] =treecompare.euclidean_distance(tree, tree2, is_bipartitions_updated=False)
BEtreePd.to_csv("B_table_out_euclidean_distance.csv")

# Compare Beast with itself with false_positives
BFPTreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu', 'BParsnpMegahit','BParsnpSpades', 'BParsnpSpadesHybrid'],
                        index=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu','BParsnpMegahit', 'BParsnpSpades', 'BParsnpSpadesHybrid'])
print(BFPTreePd)
for k, tree in BTreeDict.items():
        for k2, tree2 in BTreeDict.items():
            print(k, k2)
            BFPTreePd.loc[k][k2] =treecompare.false_positives_and_negatives(tree, tree2, is_bipartitions_updated=False)[0]
BFPTreePd.to_csv("B_table_out_false_positives.csv")

# Compare Beast with itself with false_negatives
BFNTreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu', 'BParsnpMegahit','BParsnpSpades', 'BParsnpSpadesHybrid'],
                        index=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu','BParsnpMegahit', 'BParsnpSpades', 'BParsnpSpadesHybrid'])
print(BFNTreePd)
for k, tree in BTreeDict.items():
    for k2, tree2 in BTreeDict.items():
        print(k, k2)
        BFNTreePd.loc[k][k2] =treecompare.false_positives_and_negatives(tree, tree2, is_bipartitions_updated=False)[1]
BFNTreePd.to_csv("B_table_out_false_negatives.csv")

# Compare Raxml with itself with false_negatives
BRFTreePd = pd.DataFrame(np.zeros((treeSize,treeSize)), columns=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu', 'BParsnpMegahit','BParsnpSpades', 'BParsnpSpadesHybrid'],
                        index=['BMauveCanu', 'BMauveSpades', 'BMauveSpadesHybrid', 'BParsnpCanu','BParsnpMegahit', 'BParsnpSpades', 'BParsnpSpadesHybrid'])
print(BRFTreePd)
for k, tree in BTreeDict.items():
    for k2, tree2 in BTreeDict.items():
        print(k, k2)
        BRFTreePd.loc[k][k2] =treecompare.symmetric_difference(tree, tree2, is_bipartitions_updated=False)
BRFTreePd.to_csv("B_table_out_Robinso_Foulds.csv")
