import sys
import re

sizeOfSeq = 0
conSeqDic = {}
conKey = ''
nameList = []

with open(sys.argv[1], encoding="utf8", errors='ignore') as f:
    j = 0
    for i, line in enumerate(f):
        if (line.startswith('#')):
            if (re.search("#Sequence.*File", line) != None):
                nameList.append(line.split('\t')[1].replace('\n', ''))
                conSeqDic[str(j + 1)] = ''
                j += 1
        elif (line.startswith('>')):
            conKey = line.split(':')[0][2:]
        elif (not line.startswith('>') and not line.startswith('=')):
            conSeqDic[conKey] += line.replace('\n', '')

    output = ''
    for k in sorted(conSeqDic):
        output += '>' + nameList[int(k) - 1] + '\n'
        output += conSeqDic[k] + '\n'

    with open(sys.argv[2], 'w') as f_out:
        f_out.write(output)
