import sys
import re

conSeqDic = {}
conKey = ''

with open(sys.argv[1], encoding="utf8", errors='ignore') as f:
    j = 0
    for i, line in enumerate(f):
        if (line.startswith('>')):
            conKey = line
        else:
            conSeqDic[conKey] = conSeqDic.get(conKey, "") + line.replace('\n', '')
    output = ''
    for k in conSeqDic.keys():
        output += k
        output += conSeqDic[k] + '\n'

    with open(sys.argv[2], 'w') as f_out:
        f_out.write(output)
