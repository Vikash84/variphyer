import dendropy
from dendropy.calculate import treecompare
import numpy as np
import pandas as pd
import sys

tns = dendropy.TaxonNamespace()

treeOriginal = dendropy.datamodel.treemodel.Tree.get_from_path(
    sys.argv[1],
    "newick",
    taxon_namespace=tns)

treeRaxmlBreseq = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlBreseq",
    "newick",
    taxon_namespace=tns)
treeRaxmlLofreqBowtie = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlLofreqBowtie",
    "newick",
    taxon_namespace=tns)
treeRaxmlLofreqMinimap = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlLofreqMinimap",
    "newick",
    taxon_namespace=tns)
treeRaxmlLofreqMinimapCCS = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlLofreqMinimapCCS",
    "newick",
    taxon_namespace=tns)
treeRaxmlLofreqMinimapCLR = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlLofreqMinimapCLR",
    "newick",
    taxon_namespace=tns)
treeRaxmlLofreqNGMLRCCS = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlLofreqNGMLRCCS",
    "newick",
    taxon_namespace=tns)
treeRaxmlLofreqNGMLRCLR = dendropy.datamodel.treemodel.Tree.get_from_path(
    "RaxmlLofreqNGMLRCLR",
    "newick",
    taxon_namespace=tns)


treeFasttreeBreseq = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeBreseq",
    "newick",
    taxon_namespace=tns)
treeFasttreeLofreqBowtie = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeLofreqBowtie",
    "newick",
    taxon_namespace=tns)
treeFasttreeLofreqMinimap = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeLofreqMinimap",
    "newick",
    taxon_namespace=tns)
treeFasttreeLofreqMinimapCCS = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeLofreqMinimapCCS",
    "newick",
    taxon_namespace=tns)
treeFasttreeLofreqMinimapCLR = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeLofreqMinimapCLR",
    "newick",
    taxon_namespace=tns)
treeFasttreeLofreqNGMLRCCS = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeLofreqNGMLRCCS",
    "newick",
    taxon_namespace=tns)
treeFasttreeLofreqNGMLRCLR = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeLofreqNGMLRCLR",
    "newick",
    taxon_namespace=tns)

treeOriginal.encode_bipartitions()
treeRaxmlBreseq.encode_bipartitions()
treeRaxmlLofreqBowtie.encode_bipartitions()
treeRaxmlLofreqMinimap.encode_bipartitions()
treeRaxmlLofreqMinimapCCS.encode_bipartitions()
treeRaxmlLofreqMinimapCLR.encode_bipartitions()
treeRaxmlLofreqNGMLRCCS.encode_bipartitions()
treeRaxmlLofreqNGMLRCLR.encode_bipartitions()

treeFasttreeBreseq.encode_bipartitions()
treeFasttreeLofreqBowtie.encode_bipartitions()
treeFasttreeLofreqMinimap.encode_bipartitions()
treeFasttreeLofreqMinimapCCS.encode_bipartitions()
treeFasttreeLofreqMinimapCLR.encode_bipartitions()
treeFasttreeLofreqNGMLRCCS.encode_bipartitions()
treeFasttreeLofreqNGMLRCLR.encode_bipartitions()

# dictionary
treeDict = {}
treeDict["RBreseq"] = treeRaxmlBreseq
treeDict["RLofreqBowtie"] = treeRaxmlLofreqBowtie
treeDict["RLofreqMinimap"] = treeRaxmlLofreqMinimap
treeDict["RLofreqMinimapCCS"] = treeRaxmlLofreqMinimapCCS
treeDict["RLofreqMinimapCLR"] = treeRaxmlLofreqMinimapCLR
treeDict["RLofreqNGMLRCCS"] = treeRaxmlLofreqNGMLRCCS
treeDict["RLofreqNGMLRCLR"] = treeRaxmlLofreqNGMLRCLR

treeDict["FBreseq"] = treeFasttreeBreseq
treeDict["FLofreqBowtie"] = treeFasttreeLofreqBowtie
treeDict["FLofreqMinimap"] = treeFasttreeLofreqMinimap
treeDict["FLofreqMinimapCCS"] = treeFasttreeLofreqMinimapCCS
treeDict["FLofreqMinimapCLR"] = treeFasttreeLofreqMinimapCLR
treeDict["FLofreqNGMLRCCS"] = treeFasttreeLofreqNGMLRCCS
treeDict["FLofreqNGMLRCLR"] = treeFasttreeLofreqNGMLRCLR

# beast tree
treeOriginalB = dendropy.datamodel.treemodel.Tree.get_from_path(
    "originalB.nexus",
    "nexus",
    taxon_namespace=tns)
treeBeastBreseq = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastBreseq",
    "nexus",
    taxon_namespace=tns)
treeBeastLofreqBowtie = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastLofreqBowtie",
    "nexus",
    taxon_namespace=tns)
treeBeastLofreqMinimap = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastLofreqMinimap",
    "nexus",
    taxon_namespace=tns)
treeBeastLofreqMinimapCCS = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastLofreqMinimapCCS",
    "nexus",
    taxon_namespace=tns)
treeBeastLofreqMinimapCLR = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastLofreqMinimapCLR",
    "nexus",
    taxon_namespace=tns)
treeBeastLofreqNGMLRCCS = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastLofreqNGMLRCCS",
    "nexus",
    taxon_namespace=tns)
treeBeastLofreqNGMLRCLR = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastLofreqNGMLRCLR",
    "nexus",
    taxon_namespace=tns)

treeOriginalB.encode_bipartitions()
treeBeastBreseq.encode_bipartitions()
treeBeastLofreqBowtie.encode_bipartitions()
treeBeastLofreqMinimap.encode_bipartitions()
treeBeastLofreqMinimapCCS.encode_bipartitions()
treeBeastLofreqMinimapCLR.encode_bipartitions()
treeBeastLofreqNGMLRCCS.encode_bipartitions()
treeBeastLofreqNGMLRCLR.encode_bipartitions()

# dictionary
treeDict["BBreseq"] = treeBeastBreseq
treeDict["BLofreqBowtie"] = treeBeastLofreqBowtie
treeDict["BLofreqMinimap"] = treeBeastLofreqMinimap
treeDict["BLofreqMinimapCCS"] = treeBeastLofreqMinimapCCS
treeDict["BLofreqMinimapCLR"] = treeBeastLofreqMinimapCLR
treeDict["BLofreqNGMLRCCS"] = treeBeastLofreqNGMLRCCS
treeDict["BLofreqNGMLRCLR"] = treeBeastLofreqNGMLRCLR

# compare every Raxml tree with the original tree
treeSize = len(treeDict.keys())
RPd = pd.DataFrame(np.zeros((4, treeSize)), columns=['RBreseq', 'RLofreqBowtie', 'RLofreqMinimap', 'RLofreqMinimapCCS','RLofreqMinimapCLR','RLofreqNGMLRCCS', 'RLofreqNGMLRCLR', 
'FBreseq', 'FLofreqBowtie', 'FLofreqMinimap', 'FLofreqMinimapCCS','FLofreqMinimapCLR','FLofreqNGMLRCCS', 'FLofreqNGMLRCLR',
'BBreseq', 'BLofreqBowtie', 'BLofreqMinimap', 'BLofreqMinimapCCS', 'BLofreqMinimapCLR', 'BLofreqNGMLRCCS', 'BLofreqNGMLRCLR'],
                        index=['euclidean_distance', 'false_positives', 'false_negatives', 'unweighted Robinson-Foulds'])
for k, tree in treeDict.items():
    RPd.loc['euclidean_distance'][k] = treecompare.euclidean_distance(treeOriginal, tree, is_bipartitions_updated=True)
    RPd.loc['false_positives'][k] = treecompare.false_positives_and_negatives(treeOriginal, tree, is_bipartitions_updated=True)[0]
    RPd.loc['false_negatives'][k] = treecompare.false_positives_and_negatives(treeOriginal, tree, is_bipartitions_updated=True)[1]
    RPd.loc['unweighted Robinson-Foulds'][k] = treecompare.symmetric_difference(treeOriginal, tree, is_bipartitions_updated=True)
    
RPd.to_csv(sys.argv[2])