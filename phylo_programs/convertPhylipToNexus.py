from Bio import AlignIO
from Bio.Alphabet import IUPAC, Gapped
import sys

input_handle = open(sys.argv[1], "r")
output_handle = open(sys.argv[2], "w")

alignment = AlignIO.read(input_handle, "phylip", alphabet=Gapped(IUPAC.unambiguous_dna))
output_handle.write (alignment.format("nexus"))

output_handle.close()
input_handle.close()