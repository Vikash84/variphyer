file = open("1", "r")
lines = file.readlines()
# ascii dec number
#A	65;
#T  84
#G  71
#C  67
#TypeError: 'bytes' object does not support item assignment
#atgc = {65,67,71,84, 45}
atgc = {'A', 'T', 'G', 'C', '\n'}
for line in lines:
    if (not line.startswith('>')):
        listl = list(line)
        for i in range(len(listl)):
            if listl[i] not in atgc:
                listl[i] = '-'
        line = ''.join(listl)
        print(line)

# Algorithm: for all the encodings which are not encoding of ATGC, convert to '-'
'''import io

with io.open("2", 'r', encoding='cp1252') as f:
    for line in f:
        print(line.rstrip('\n\x00'))'''