import sys
import re

sizeOfSeq = 0
conSeqDic = {}
conKey = ''
nameList = []
atgc = {'A', 'T', 'G', 'C', '-'}
#atgc = {65,67,71,84,45}

#read the file as ascii chars, repalce the unknown encoding with '?'
with open(sys.argv[1], 'rb') as f:
    j = 0
    current = 0
    previous = 0
    for i, line in enumerate(f):
        if (line.startswith(b'#')):
            if (re.search(b"#Sequence.*File", line) != None):
                nameList.append(line.split(b'\t')[1].replace(b'\n', b''))
                conSeqDic[str(j + 1)] = ''
                j += 1
        elif (line.startswith(b'>')):
            conKey = line.split(b':')[0][2:].decode("utf-8", "replace")
            if(previous != current and previous != 0):
                print('current=', current, ' previous=', previous)
                print('The row before this line has at least one extra character', line)
                sys.exit()
            else:
                previous = current
                current = 0
        elif (not line.startswith(b'>') and not line.startswith(b'=')):
            cleanLine = line.replace(b'\n', b'')
            conSeqDic[conKey] += cleanLine
            print(len(cleanLine))
            current += len(cleanLine)
        elif (line.startswith(b'=')):
            current = 0
            previous = 0

    output = ''
    for k in sorted(conSeqDic):
        output += b'>' + nameList[int(k) - 1] + b'\n'
        line = conSeqDic[k]
        listl = list(line)
        for i in range(len(listl)):
            if listl[i] not in atgc:
                listl[i] = b'-'
        line = b''.join(listl)
        output += line + b'\n'

    with open(sys.argv[2], 'w') as f_out:
        f_out.write(output)
