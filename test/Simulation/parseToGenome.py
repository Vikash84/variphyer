import sys
import re

with open(sys.argv[1]) as f:
    lines = f.readlines()
    n = len(lines)
    print(n)
    for i in range(1,n):
        st = ''
        line = lines[i]
        line = re.sub(r'Taxon\d+\s+', '', line)
        st += '>NC_016845.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 chromosome, complete genome\n'
        st += line[0:5333942]+'\n'
        st += '>NC_016838.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 plasmid pKPHS1, complete sequence\n'
        st += line[5333942:5456741]+'\n'
        st += '>NC_016846.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 plasmid pKPHS2, complete sequence\n'
        st += line[5456741:5567936]+'\n'
        st += '>NC_016839.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 plasmid pKPHS3, complete sequence\n'
        st += line[5567936:5673910]+'\n'
        st += '>NC_016840.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 plasmid pKPHS4, complete sequence\n'
        st += line[5673910:5677661]+'\n'
        st += '>NC_016847.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 plasmid pKPHS5, complete sequence\n'
        st += line[5677661:5681014]+'\n'
        st += '>NC_016841.1 Klebsiella pneumoniae subsp. pneumoniae HS11286 plasmid pKPHS6, complete sequence\n'
        st += line[5681014:5682322]
        with open('./genome_'+str(i)+'.fasta','w') as f_out:
            f_out.write(st)