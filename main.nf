#!/usr/bin/env nextflow
/*
========================================================================================
                         Variphyer
========================================================================================
 Benchmark Evaluation Pipeline.
 Author: Chunxiao Liao
 #### Wikipage
 https://gitlab.com/treangenlab/variphyer/-/wikis/home
----------------------------------------------------------------------------------------
*/

/*
 * Define helpMessage
 */

 def helpMessage() {

   log.info"""

   Usage:

     The typical command for running the pipeline is as follows:

       nextflow run main.nf [read type] [mode] [optional path]

     Mandatory arguments:
       --s, --r or --a               running mode, --s is simulate from genome, --r is to start from reads, --a is to start from alignment from either contigs or genomes
       --longMode, or --shortMode            reads type, --longMode is to run long reads workflow, --shortMode is to run short reads workflow

     Options:
        --ref                        Genomes used for simulation
        --oneRef                     Reference genome dir, default is "refSeq/GCF_000240185.1_ASM24018v2_genomic.fna"
        --outputDir                  The output directory where the results will be saved
        --program                    Script full path, for example [your-full-path]/phylo_programs/
        --shortPath                  Directory of illumina pair-end reads, the format could be for example: "[path-to-files]/*/*{_1,2}.f*"
        --longPath                   Directory of long reads, for example: "[path-to-files]/*/*.fast*"
        --longAlignmentPath          Directory of long contigs
        --shortAlignmentPath         Directory of short contigs
        --test                       To test the all the unit tests to make sure the installation is succeed

     Other options:
       --help                        Show this message and exit.

     Softwares and dependencies:
         MEGAHIT v1.2.3-beta
         Canu v2.0
         parsnp v1.5
         mauve_linux_snapshot_2015-02-13
         stripSubsetLCBs
         RAxML
         SPAdes-3.13.1-Linux
         harvesttools-Linux64-v1.2
         artsrcmountrainier2016.06.05linux
         gsl-2.5
         PBSIM-PacBio-Simulator v1.0.4
         biopython
         BEASTv1.10.4
         BEASTGen_v1.0.2
         beagle-lib
         Trimmomatic-0.39
         mash v1.1
         FastTree v2.1.10
         numpy
         fastani
         pybedtools
         scipy
         scikit-learn
         htseq
         lofreq v2.1.3.1
         minimap2v2.17-r941
         bowtie2-align-s v2.3.5.1
         ngmlr v0.2.7
         samtools v1.3.1
         sniffles v1.0.12
         breseq v0.35.2
         MAFFT v7.471
    """.stripIndent()
 }

// SET UP CONFIGURATION VARIABLES.

// Show help message.
if (params.help){
  helpMessage()
  exit 0
}

oneRef = file(params.oneRef)

//original tree
treeR = file(params.originalTreeR)
tree = file(params.originalTree)

//create empty channels
samples_ch_pbhifi = Channel.empty()
samples_ch_art = Channel.empty()
samples_ch_pb = Channel.empty()
art_file_ch_trim = Channel.empty()
art_file_ch_minimap = Channel.empty()
art_file_ch_bowtie = Channel.empty()
art_file_ch_breseq = Channel.empty()

read_ch_pbhifi = Channel.empty()
read_ch_NGMLR = Channel.empty()
read_ch_NGMLR_CLR = Channel.empty()
read_ch_preprocessSniffles = Channel.empty()
read_ch_preprocessFai = Channel.empty()

//simulate mode
//samples_ch_art is short channel
//samples_ch_pbhifi is long simulate channel
if(params.s) {
Channel
    .fromPath( params.ref )
    .map { file -> tuple(file.baseName, file) }
    .ifEmpty { error "Oops! Cannot find any file matching: ${params.ref}"  }
    .into { samples_ch_art; samples_ch_pbhifi; samples_ch_pb }
}

//validate input
if(params.s && params.r || params.s && params.a || params.r && params.a) {
    exit 1, "Oops! Cannot set more than one mode from: --s=true, --r=true, --a=true"
}

if((params.s || params.r || params.a) == false){
    exit 1, "Oops! Please set at least one mode parameter from: --s=true, --r=true, --a=true"
}

if((params.longMode || params.shortMode) == false) {
    exit 1, "Oops! Please set at least one reads type parameter from: --longMode=true, --shortMode=true"
}

// set channel to short reads
if(params.r && params.shortMode){
    Channel
        .fromFilePairs( params.shortPath, flat: true)
        .ifEmpty { error "Oops! Cannot find any file matching: ${params.shortPath}"  }
        .into { art_file_ch_trim; art_file_ch_minimap; art_file_ch_bowtie; art_file_ch_breseq}
} else {
    art_file_ch_trim = Channel.empty()
    art_file_ch_minimap = Channel.empty()
    art_file_ch_bowtie = Channel.empty()
    art_file_ch_breseq = Channel.empty()
}

// set channel to long reads
// use read_CLR_ch_minimap if it is CLR
// use read_CCS_ch_minimap is it is CCS
if(params.r && params.longMode){
    Channel
    .fromPath( params.longPath )
    .map { file -> tuple(file.baseName, file) }
    .ifEmpty { error "Oops! Cannot find any file matching: ${params.ref}"  }
    .into { read_ch_pbhifi; read_ch_NGMLR; read_ch_NGMLR_CLR; read_ch_preprocessSniffles; read_ch_preprocessFai; read_CLR_ch_minimap; read_CCS_ch_minimap}
} else{
    read_ch_pbhifi = Channel.empty()
    read_ch_NGMLR = Channel.empty()
    read_ch_NGMLR_CLR = Channel.empty()
    read_ch_preprocessSniffles = Channel.empty()
    read_ch_preprocessFai = Channel.empty()
    read_CLR_ch_minimap = Channel.empty()
    read_CCS_ch_minimap = Channel.empty()
}

// set channel to long alignment
if(params.a && params.longMode){
Channel
    .fromPath(params.longAlignmentPath)
    .ifEmpty { error "Oops! Cannot find any file matching: ${params.longAlignmentPath}"  }
    .into {long_align_ch_mauve; long_align_ch_parsnp}
} else {
    long_align_ch_mauve = Channel.empty()
    long_align_ch_parsnp = Channel.empty()
}

// set channel to short alignment
if(params.a && params.shortMode){
Channel
    .fromPath(params.shortAlignmentPath)
    .ifEmpty { error "Oops! Cannot find any file matching: ${params.shortAlignmentPath}"  }
    .into {short_align_ch_parsnp_megahit;short_align_ch_mauve_spades; short_align_ch_mauve_spades_hybrid;short_align_ch_parsnp_spades; short_align_ch_parsnp_hybrid}
} else {
    short_align_ch_parsnp_megahit = Channel.empty()
    short_align_ch_parsnp_spades = Channel.empty()
    short_align_ch_mauve_spades = Channel.empty()
    short_align_ch_mauve_spades_hybrid = Channel.empty()
    short_align_ch_parsnp_hybrid = Channel.empty()
}

//highly accurate (99.8%) long high-fidelity (HiFi) reads with an average length of 13.5 kilo- bases (kb).
process pbSim_HiFi {
    publishDir "${params.outputDir}/pbSim_HiFi/${genome.baseName}", mode: 'copy'
    tag "${replicateId}"
    input:
    set replicateId, file(genome) from samples_ch_pbhifi// naming genome is confusing in this context

    output:
    set replicateId, file("${replicateId}_reads.fastq") into pb_optional_hifi_ch_minimapCCS, pb_optional_hifi_ch_NGMLR, pb_optional_hifi_ch_canu

    when:
    params.s && params.longMode

    script:
    """
    pbsim --data-type CCS --depth 20 --length-min 500 --length-max 23000 --length-mean 13000 --accuracy-min 0.98 --accuracy-mean 0.998 --model_qc /phylo_programs/PBSIM-PacBio-Simulator/data/model_qc_ccs $genome;
    cat sd_000*.fastq > ${replicateId}_reads.fastq;
    rm sd_000*.fastq;
    rm sd_000*.maf;
    """
}

// pcbio normal
process pbSim{
    publishDir "${params.outputDir}/pbSim_normal/${genome.baseName}", mode: 'copy'
    tag "${replicateId}"
    input:
    set replicateId, file(genome) from samples_ch_pb// naming genome is confusing in this context

    output:
    set replicateId, file("${replicateId}_reads.fastq") into pb_CLR_ch_spades_hybrid, pb_optional_CLR_ch_minimap, pb_optional_CLR_ch_NGMLR

    when:
    params.s && params.longMode

    script:
    """
    pbsim --data-type CLR --depth 20 --model_qc /phylo_programs/PBSIM-PacBio-Simulator/data/model_qc_clr $genome;
    cat sd_000*.fastq > ${replicateId}_reads.fastq;
    rm sd_000*.fastq;
    rm sd_000*.maf;
    """
}

process art_illumina_process {
    tag "${replicateId}"
    publishDir "${params.outputDir}/art_illumina/${genome.baseName}", mode: 'copy'

    input:
    set replicateId, file(genome) from samples_ch_art

    output:
    set replicateId, file("${replicateId}read_1*"), file("${replicateId}read_2*") into art_optional_ch_bowtie, art_optional_ch_trim, art_optional_ch_minimap, art_optional_ch_breseq

    when:
    params.s && params.shortMode == true
    script:
    """
    art_illumina -ss HS25 -sam -i $genome -p -l 100 -f 20 -m 200 -s 10 -f 10 -o ${replicateId}read_;
    rm *.sam;
    rm *.aln;
    rm *.fasta;
    """
}

//reads trimming
process trim_illumina{
    tag "${replicateId}"
    publishDir "${params.outputDir}/trim_illumina", mode: 'copy'

    input:
    set replicateId, file(read_1), file(read_2) from art_file_ch_trim.mix(art_optional_ch_trim)

    output:
    set replicateId, file("${replicateId}_1.fq.gz"), file("${replicateId}_2.fq.gz") into trimReceiver1, trimReceiver2, trimReceiver3

    script:
    """
    java -jar /phylo_programs/Trimmomatic-0.39/trimmomatic-0.39.jar PE ${read_1} ${read_2} ${replicateId}_1.fq.gz output_forward_unpaired.fq.gz ${replicateId}_2.fq.gz output_reverse_unpaired.fq.gz ILLUMINACLIP:/phylo_programs/Trimmomatic-0.39/adapters/TruSeq3-PE.fa:2:30:10:2:keepBothReads LEADING:3 TRAILING:3 MINLEN:36;
    """
}

process preprocess_ref {
    input:
    file ref from oneRef
    output:
    file "*.fai" into fai1, fai2, fai3, fai4, fai5, fai6

    script:
    """
    samtools faidx $ref
    """
}

//minimap2 illumina
process minimap2_illumina{
    tag "${replicateId}"
    publishDir "${params.outputDir}/minimap2", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(read_1), file(read_2) from art_file_ch_minimap.mix(art_optional_ch_minimap)
    file ref from oneRef

    output:
    set replicateId, file("${replicateId}.bam"), file("${replicateId}.bam.bai") into minimap2IlluminaReceiver, minimap2IlluminaLofreqReceiver
    script:
    """
    minimap2 -ax sr $ref $read_1 $read_2 | samtools view -bS - | samtools sort - > ${replicateId}.bam;
    samtools index ${replicateId}.bam;
    """
}

// minimap2 pacbio
// need to sort for sniffles
process minimap2_pacbio_CLR{
    tag "${replicateId}"
    publishDir "${params.outputDir}/minimap2PacbioCLR", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(reads) from read_CLR_ch_minimap.mix(pb_optional_CLR_ch_minimap)
    file ref from oneRef

    output:
    set replicateId, file("${replicateId}.bam"), file("${replicateId}.bam.bai") into minimap2PacbioCLRReceiver

    script:
    """
    minimap2 -ax map-pb $ref $reads | samtools view -bS - | samtools sort - > ${replicateId}.bam;
    samtools index ${replicateId}.bam;
    """
}

// minimap pacbio CCS
// need sort for sniffles
process minimap2_pacbio_CCS{
    tag "${replicateId}"
    publishDir "${params.outputDir}/minimap2PacbioCCS", mode: 'copy'

    input:
    set replicateId, file(reads) from read_CCS_ch_minimap.mix(pb_optional_hifi_ch_minimapCCS)
    file ref from oneRef
    maxForks 4

    output:
    set replicateId, file("${replicateId}.bam"), file("${replicateId}.bam.bai") into minimap2PacbioCCSReceiver
    script:
    """
    minimap2 -ax asm20 $ref $reads | samtools view -bS - | samtools sort - > ${replicateId}.bam;
    samtools index ${replicateId}.bam;
    """
}

// Mapping bowtie, bowtie performs best for short reads
process bowtie_illumina{

    tag "${replicateId}"
    publishDir "${params.outputDir}/bowtieIllumina", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(read_1), file(read_2) from art_file_ch_bowtie.mix(art_optional_ch_bowtie)
    file ref from oneRef

    output:
    set replicateId, file("${replicateId}.bam"), file("${replicateId}.bam.bai") into bowtie2Receiver

    script:
    """
    bowtie2-build $ref index;
    bowtie2 -x index -1 $read_1 -2 $read_2 | samtools view -bS - | samtools sort - > ${replicateId}.bam;
    samtools index ${replicateId}.bam;
    """
}

//NGMLR ?
process NGMLR_CCS{
    tag "${replicateId}"
    publishDir "${params.outputDir}/NGMLR_CCS", mode:'copy'
    maxForks 4

    input:
    set replicateId, file(reads) from read_ch_NGMLR.mix(pb_optional_hifi_ch_NGMLR)
    file ref from oneRef

    output:
    set replicateId, file("${replicateId}.bam"), file("${replicateId}.bam.bai") into ngmlr_bam_CCS_sniffles, ngmlr_bam_CCS_lofreq

    script:
    """
    ngmlr -t 4 -r $ref -q $reads | samtools view -bS - | samtools sort - > ${replicateId}.bam;
    samtools index ${replicateId}.bam;
    """
}

//NGMLR
process NGMLR_CLR{
    //cpus = 1
    tag "${replicateId}"
    publishDir "${params.outputDir}/NGMLR_CLR", mode:'copy'
    maxForks 4

    input:
    set replicateId, file(reads) from read_ch_NGMLR_CLR.mix(pb_optional_CLR_ch_NGMLR)
    file ref from oneRef

    output:
    set replicateId, file("${replicateId}.bam"), file("${replicateId}.bam.bai") into ngmlr_bam_CLR_lofreq, ngmlr_bam_CLR_sniffles

    script:
    """
    ngmlr -t 4 -r $ref -q $reads | samtools view -bS - | samtools sort - > ${replicateId}.bam;
    samtools index ${replicateId}.bam; 
    """
}

// Sniffles performs best with the mappings of NGMLR our novel long read mapping method.
// Sniffles is a structural variation caller using third generation sequencing (PacBio or Oxford Nanopore)
// sort by samtools: samtools sort sample.bam -o sample.sorted.bam
process sniffles_ngmlr_pacbio_CLR {
    tag "${replicateId}"
    publishDir "${params.outputDir}/sniffles_ngmlr_CLR", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from ngmlr_bam_CLR_sniffles

    output:
    file("sniffles_${replicateId}.vcf") into sniffles_NGMLR_CLR_vcf

    """
    #--threads ${task.cpus}
    sniffles --threads 8 --mapped_reads $bam --vcf sniffles_${replicateId}.vcf
    """
}

process merge_sniffles_ngmlr_pacbio_CLR {
    publishDir "${params.outputDir}/snifflesNgmlrCLRMerge", mode: 'copy'
    input:
        file "*" from sniffles_NGMLR_CLR_vcf.collect()
    output:
        file "snifflesNgmlrPacbioCLRMerge/*.vcf" into snifflesNgmlrPacbioCLRMerge

    script:
    """
    mkdir snifflesNgmlrPacbioCLRMerge;
    scp *.vcf snifflesNgmlrPacbioCLRMerge;
    """
}

process vcf_mfa_sniffles_ngmlr_CLR {
    publishDir "${params.outputDir}/vcf_mfa_sniffles_ngmlr_CLR", mode: 'copy'
    input:
        file vcf from snifflesNgmlrPacbioCLRMerge
        file ref from oneRef
    output:
        file "result.mfa" into snifflesNGMLRCLROutputMfa1, snifflesNGMLRCLROutputMfa2, snifflesNGMLRCLROutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

/*
process mfa_raxml_sniffles_NGMLR_CLR {
    publishDir "${params.outputDir}/RaxmlsnifflesNGMLRCLR", mode: 'copy'
    input:
        file mfa from snifflesNGMLRCLROutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_sniffles_NGMLR_CLR
    script:
        "raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20";
}

process mfa_beast_sniffles_NGMLR_CLR {
    publishDir "${params.outputDir}/beast_sniffles_NGMLR_CLR", mode: 'copy'
    input:
        file mfa from snifflesNGMLRCLROutputMfa2
    output:
        file "out.txt" into beast_sniffles_NGMLR_CLR
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        """
}*/

// Sniffles performs best with the mappings of NGMLR our novel long read mapping method.
// Sniffles is a structural variation caller using third generation sequencing (PacBio or Oxford Nanopore)
// sort by samtools: samtools sort sample.bam -o sample.sorted.bam
process sniffles_ngmlr_pacbio_CCS {
    tag "${replicateId}"
    publishDir "${params.outputDir}/sniffles_ngmlr_CCS", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from ngmlr_bam_CCS_sniffles

    output:
    file("sniffles_${replicateId}.vcf") into sniffles_NGMLR_CCS_vcf

    """
    #--threads ${task.cpus}
    sniffles --threads 8 --mapped_reads $bam --vcf sniffles_${replicateId}.vcf
    """
}

process merge_sniffles_ngmlr_pacbio_CCS {
    publishDir "${params.outputDir}/snifflesNgmlrCLRMerge", mode: 'copy'
    input:
        file "*" from sniffles_NGMLR_CCS_vcf.collect()
    output:
        file "snifflesNgmlrPacbioCCSMerge/*.vcf" into snifflesNgmlrPacbioCCSMerge

    script:
    """
    mkdir snifflesNgmlrPacbioCCSMerge;
    scp *.vcf snifflesNgmlrPacbioCCSMerge;
    """
}

process vcf_mfa_sniffles_ngmlr_CCS {
    publishDir "${params.outputDir}/vcf_mfa_sniffles_ngmlr_CCS", mode: 'copy'
    input:
        file vcf from snifflesNgmlrPacbioCCSMerge
        file ref from oneRef
    output:
        file "result.mfa" into snifflesNGMLRCCSOutputMfa1, snifflesNGMLRCCSOutputMfa2, snifflesNGMLRCCSOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}
/*
process mfa_raxml_sniffles_NGMLR_CCS {
    publishDir "${params.outputDir}/RaxmlsnifflesNGMLRCCS", mode: 'copy'
    input:
        file mfa from snifflesNGMLRCCSOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_sniffles_NGMLR_CCS
    script:
        "raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20";
}

process mfa_beast_sniffles_NGMLR_CCS {
    publishDir "${params.outputDir}/beast_sniffles_NGMLR_CCS", mode: 'copy'
    input:
        file mfa from snifflesNGMLRCCSOutputMfa2
    output:
        file "out.txt" into beast_sniffles_NGMLR_CCS
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        """
}*/

//computational pipeline for the analysis of short-read re-sequencing data (e.g. Illumina, 454, IonTorrent, etc.).
//It uses reference-based alignment approaches to predict mutations in a sample relative to an already sequenced genome.
//breseq uses Bowtie2 to map reads to the reference genome sequence:
process breseq_illumina {
    tag "${replicateId}"
    publishDir "${params.outputDir}/breseqIllumina", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(read_1), file(read_2) from art_file_ch_breseq.mix(art_optional_ch_breseq)
    file ref from oneRef

    output:
    file("${replicateId}_output.vcf") into breseq_output_ch

    script:
    """
    breseq -p -r $ref $read_1 $read_2 -j 4;
    mv output/output.vcf ./${replicateId}_output.vcf
    """
}

process merge_breseq_illumina {

    publishDir "${params.outputDir}/breseqOutputMerge", mode: 'copy'
    input:
        file "*" from breseq_output_ch.collect()
    output:
        file "breseqOutputMerge/*.vcf" into breseqOutputMerge

    script:
        """
        mkdir breseqOutputMerge
        mv *.vcf breseqOutputMerge
        """
}

process vcf_mfa_breseq {
    publishDir "${params.outputDir}/vcf_mfa_breseq", mode: 'copy'
    input:
        file vcf from breseqOutputMerge
        file ref from oneRef
    output:
        file "result.mfa" into breseqOutputMfa1, breseqOutputMfa2, breseqOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_breseq {
    publishDir "${params.outputDir}/RaxmlBreseq", mode: 'copy'
    input:
        file mfa from breseqOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_breseq1, Raxml_breseq2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_output.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_breseq {
    publishDir "${params.outputDir}/beastBreseq", mode: 'copy'
    input:
        file mfa from breseqOutputMfa2
    output:
        file "out.txt" into beast_breseq1, beast_breseq2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_output.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}

//lofreq
//Assuming your BAM file (aln.bam) contains reads aligned against the sequence/s in ref.fa and you want to predict variants and save them to vars.vcf
//If you also want to call indels add --call-indels to the parameter list.
process lofreq_illumina_minimap {
    tag "${replicateId}"
    publishDir "${params.outputDir}/lofreqIlluminaMinimap", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from minimap2IlluminaLofreqReceiver
    file ref from oneRef
    file fai from fai1

    output:
    file("${replicateId}_lofreq.vcf") into lofreq_minimap_output_ch

    script:
    """
    lofreq call-parallel --call-indels -f $ref -o ${replicateId}_lofreq.vcf $bam --pp-threads 8;
    """
}

process merge_lofreq_illumina_minimap {
    publishDir "${params.outputDir}/lofreqMinimapOutputChMerge", mode: 'copy'
    input:
        file "*" from lofreq_minimap_output_ch.collect()
    output:
        file "lofreqMinimapOutputChMerge/*.vcf" into lofreqMinimapOutputChMerge

    script:
    """
    mkdir lofreqMinimapOutputChMerge;
    scp *_lofreq.vcf lofreqMinimapOutputChMerge;
    """
}

//Illumina
process vcf_mfa_lofreq_Minimap {
    publishDir "${params.outputDir}/vcf_mfa_lofreq_Minimap", mode: 'copy'
    input:
        file vcf from lofreqMinimapOutputChMerge
        file ref from oneRef
    output:
        file "result.mfa" into lofreqMinimapOutputMfa1, lofreqMinimapOutputMfa2, lofreqMinimapOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_lofreqMinimap {
    publishDir "${params.outputDir}/RaxmlLofreqMinimap", mode: 'copy'
    input:
        file mfa from lofreqMinimapOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_lofreq_Minimap1, Raxml_lofreq_Minimap2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_lofreq.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_lofreqMinimap {
    publishDir "${params.outputDir}/beastLofreqMinimap", mode: 'copy'
    input:
        file mfa from lofreqMinimapOutputMfa2
    output:
        file "out.txt" into beast_lofreq_Minimap1, beast_lofreq_Minimap2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_lofreq.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}

//lofreq minimap pacbio CLR
process lofreq_pacbio_CLR_minimap {
    tag "${replicateId}"
    publishDir "${params.outputDir}/lofreqPacbioCLRMimimap", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from minimap2PacbioCLRReceiver
    file ref from oneRef
    file fai from fai2

    output:
    file("${replicateId}_lofreq.vcf") into lofreq_minimap_pacbio_output_ch

    script:
    """
    lofreq call-parallel --call-indels -f $ref -o ${replicateId}_lofreq.vcf $bam --pp-threads 8;
    """
}

process merge_lofreq_pacbio_CLR_minimap {
    publishDir "${params.outputDir}/lofreqPacbioCLRMinimapMerge", mode: 'copy'
    input:
        file "*" from lofreq_minimap_pacbio_output_ch.collect()
    output:
        file "lofreqPacbioCLRMinimapMerge/*.vcf" into lofreqPacbioCLRMinimapMerge

    script:
    """
    mkdir lofreqPacbioCLRMinimapMerge;
    scp *_lofreq.vcf lofreqPacbioCLRMinimapMerge;
    """
}

process vcf_mfa_lofreq_CLR_Minimap {
    publishDir "${params.outputDir}/vcf_mfa_lofreq_CLR_Minimap", mode: 'copy'
    input:
        file vcf from lofreqPacbioCLRMinimapMerge
        file ref from oneRef
    output:
        file "result.mfa" into lofreqPacbioCLRMinimapOutputMfa1, lofreqPacbioCLRMinimapOutputMfa2, lofreqPacbioCLRMinimapOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_lofreqMinimapCLR {
    publishDir "${params.outputDir}/RaxmlLofreqMinimapCLR", mode: 'copy'
    input:
        file mfa from lofreqPacbioCLRMinimapOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_lofreq_Minimap_CLR1, Raxml_lofreq_Minimap_CLR2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_lofreq.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_lofreqMinimapCLR {
    publishDir "${params.outputDir}/beastLofreqMinimapCLR", mode: 'copy'
    input:
        file mfa from lofreqPacbioCLRMinimapOutputMfa2
    output:
        file "out.txt" into beast_lofreq_Minimap_CLR1, beast_lofreq_Minimap_CLR2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_lofreq.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}

//lofreq minimap pacbio CCS
process lofreq_pacbio_CCS_minimap {
    tag "${replicateId}"
    publishDir "${params.outputDir}/lofreqPacbioCCSMimimap", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from minimap2PacbioCCSReceiver
    file ref from oneRef
    file fai from fai3

    output:
    file("${replicateId}_lofreq.vcf") into lofreq_CCS_minimap_pacbio_output_ch

    script:
    """
    lofreq call-parallel --call-indels -f $ref -o ${replicateId}_lofreq.vcf $bam --pp-threads 8;
    """
}

process merge_lofreq_pacbio_CCS_minimap {
    publishDir "${params.outputDir}/lofreqPacbioCCSMinimapMerge", mode: 'copy'
    input:
        file "*" from lofreq_CCS_minimap_pacbio_output_ch.collect()
    output:
        file "lofreqPacbioCCSMinimapMerge/*.vcf" into lofreqPacbioCCSMinimapMerge

    script:
    """
    mkdir lofreqPacbioCCSMinimapMerge;
    scp *_lofreq.vcf lofreqPacbioCCSMinimapMerge;
    """
}

process vcf_mfa_lofreq_CCS_Minimap {
    publishDir "${params.outputDir}/vcf_mfa_lofreq_CCS_Minimap", mode: 'copy'
    input:
        file vcf from lofreqPacbioCCSMinimapMerge
        file ref from oneRef
    output:
        file "result.mfa" into lofreqPacbioCCSMinimapOutputMfa1, lofreqPacbioCCSMinimapOutputMfa2, lofreqPacbioCCSMinimapOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_lofreq_CCS_minimap {
    publishDir "${params.outputDir}/RaxmlLofreqMinimapCCS", mode: 'copy'
    input:
        file mfa from lofreqPacbioCCSMinimapOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_vcf_mfa_lofreq_CCS_Minimap1, Raxml_vcf_mfa_lofreq_CCS_Minimap2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_lofreq.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_lofreq_CCS_minimap {
    publishDir "${params.outputDir}/beastLofreqMinimapCCS", mode: 'copy'
    input:
        file mfa from lofreqPacbioCCSMinimapOutputMfa2
    output:
        file "out.txt" into beast_vcf_mfa_lofreq_CCS_Minimap1, beast_vcf_mfa_lofreq_CCS_Minimap2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_lofreq.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}

//lofreq_bowtie
process lofreq_illumina_bowtie {
    tag "${replicateId}"
    publishDir "${params.outputDir}/lofreqIlluminaBowtie", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from bowtie2Receiver
    file ref from oneRef
    file fai from fai4

    output:
    file("${replicateId}_lofreq.vcf") into lofreq_bowtie_output_ch

    script:
    """
    lofreq call-parallel --call-indels -f $ref -o ${replicateId}_lofreq.vcf $bam --pp-threads 8;
    """
}

process merge_lofreq_illumina_bowtie {
    publishDir "${params.outputDir}/lofreqIlluminaBowtiepMerge", mode: 'copy'
    input:
        file "*" from lofreq_bowtie_output_ch.collect()
    output:
        file "lofreqMerge/*.vcf" into lofreqBowtieVcfMerge

    script:
    """
    mkdir lofreqMerge;
    scp *_lofreq.vcf lofreqMerge;
    """
}

process vcf_mfa_lofreq_illumina_bowtie {
    publishDir "${params.outputDir}/vcfMfaLofreqIlluminaBowtie", mode: 'copy'
    input:
        file vcf from lofreqBowtieVcfMerge
        file ref from oneRef
    output:
        file "result.mfa" into lofreqIlluminaBowtieOutputMfa1, lofreqIlluminaBowtieOutputMfa2, lofreqIlluminaBowtieOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_lofreq_bowtie {
    publishDir "${params.outputDir}/RaxmlLofreqBowtie", mode: 'copy'
    input:
        file mfa from lofreqIlluminaBowtieOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_vcf_mfa_lofreq_bowtie1, Raxml_vcf_mfa_lofreq_bowtie2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_lofreq.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_lofreq_bowtie {
    publishDir "${params.outputDir}/beastLofreqBowtie", mode: 'copy'
    input:
        file mfa from lofreqIlluminaBowtieOutputMfa2
    output:
        file "out.txt" into beast_vcf_mfa_lofreq_bowtie1, beast_vcf_mfa_lofreq_bowtie2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_lofreq.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}

//lofreq NGMLR
process lofreq_NGMLR_CLR {
    tag "${replicateId}"
    publishDir "${params.outputDir}/lofreqNGMLR_CLR", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from ngmlr_bam_CLR_lofreq
    file ref from oneRef
    file fai from fai5

    output:
    file("${replicateId}_lofreq.vcf") into lofreq_NGMLR_output_CLR_ch

    script:
    """
    lofreq call-parallel --call-indels -f $ref -o ${replicateId}_lofreq.vcf $bam --pp-threads 8;
    """
}

process merge_lofreq_NGMLR_CLR {
    publishDir "${params.outputDir}/lofreqNGMLRCLRMerge", mode: 'copy'
    input:
        file "*" from lofreq_NGMLR_output_CLR_ch.collect()
    output:
        file "lofreqNGMLROutputCLRMerge/*.vcf" into lofreqNGMLROutputCLRMerge

    script:
    """
    mkdir lofreqNGMLROutputCLRMerge;
    scp *_lofreq.vcf lofreqNGMLROutputCLRMerge;
    """
}

process vcf_mfa_lofreq_NGMLR_CLR {
    publishDir "${params.outputDir}/vcfMfaLofreqNGMLRCLR", mode: 'copy'
    input:
        file vcf from lofreqNGMLROutputCLRMerge
        file ref from oneRef
    output:
        file "result.mfa" into lofreqNGMLRCLROutputMfa1, lofreqNGMLRCLROutputMfa2, lofreqNGMLRCLROutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_lofreq_NGMLR_CLR {
    publishDir "${params.outputDir}/RaxmlLofreqNGMLRCLR", mode: 'copy'
    input:
        file mfa from lofreqNGMLRCLROutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_lofreq_NGMLR_CLR1, Raxml_lofreq_NGMLR_CLR2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_lofreq.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_lofreq_NGMLR_CLR {
    publishDir "${params.outputDir}/beastLofreqNGMLRCLR", mode: 'copy'
    input:
        file mfa from lofreqIlluminaBowtieOutputMfa2
    output:
        file "out.txt" into Beast_lofreq_NGMLR_CLR1, Beast_lofreq_NGMLR_CLR2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_lofreq.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}

//lofreq NGMLR CCS
process lofreq_NGMLR_CCS {
    tag "${replicateId}"
    publishDir "${params.outputDir}/lofreqNGMLR_CCS", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(bam), file(bai) from ngmlr_bam_CCS_lofreq
    file ref from oneRef
    file fai from fai6

    output:
    file("${replicateId}_lofreq.vcf") into lofreq_NGMLR_output_CCS_ch

    script:
    """
    lofreq call-parallel --call-indels -f $ref -o ${replicateId}_lofreq.vcf $bam --pp-threads 8;
    """
}

process merge_lofreq_NGMLR_CCS {
    publishDir "${params.outputDir}/lofreqNGMLRCCSMerge", mode: 'copy'
    input:
        file "*" from lofreq_NGMLR_output_CCS_ch.collect()
    output:
        file "lofreqNGMLROutputCCSMerge/*.vcf" into lofreqNGMLROutputCCSMerge

    script:
    """
    mkdir lofreqNGMLROutputCCSMerge;
    scp *_lofreq.vcf lofreqNGMLROutputCCSMerge;
    """
}

process vcf_mfa_lofreq_NGMLR_CCS {
    publishDir "${params.outputDir}/vcfMfaLofreqNGMLRCCS", mode: 'copy'
    input:
        file vcf from lofreqNGMLROutputCCSMerge
        file ref from oneRef
    output:
        file "result.mfa" into lofreqNGMLRCCSOutputMfa1, lofreqNGMLRCCSOutputMfa2, lofreqNGMLRCCSOutputMfa3

    shell:
    '''
    mkdir vcfDir
    mv *.vcf vcfDir
    python !{params.program}mergeVcf.py vcfDir .
    mkdir chrom
    cat !{ref} | awk '{if(substr($0, 1, 1)==">") {filename="chrom/" (substr($0, 2, 9) ".fa")} print $0 > filename}' #split reference into chromsomes
    cd chrom
    for f in *.fa ; do harvesttools -v ../out.vcf -f $f -M "$f.mfa"; done # generate vcf->mfa for each chromesome
    rm *.fa
    cat *.mfa > total.mfa
    python !{params.program}concatenateMfa.py total.mfa result.mfa
    mv result.mfa ..
    cd ..
    rm -r chrom
    '''
}

process mfa_raxml_lofreq_NGMLR_CCS {
    publishDir "${params.outputDir}/RaxmlLofreqNGMLRCCS", mode: 'copy'
    input:
        file mfa from lofreqNGMLRCCSOutputMfa1
    output:
        file "RAxML_bestTree._raxml" into Raxml_lofreq_NGMLR_CCS1, Raxml_lofreq_NGMLR_CCS2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/_lofreq.vcf//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        """
}

process mfa_beast_lofreq_NGMLR_CCS {
    publishDir "${params.outputDir}/beastLofreqNGMLRCCS", mode: 'copy'
    input:
        file mfa from lofreqNGMLRCCSOutputMfa2
    output:
        file "out.txt" into beast_lofreq_NGMLR_CCS1, beast_lofreq_NGMLR_CCS2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/_lofreq.vcf//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        """
}


process fasttree_lofreq_NGMLR_CCS {
    publishDir "${params.outputDir}/fasttree_lofreq_NGMLR_CCS", mode: 'copy'
    input:
        file mfa from lofreqNGMLRCCSOutputMfa3
    output:
        file "tree_file" into fasttree_lofreqNGMLRCCS
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_lofreq.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    """
}

process fasttree_lofreq_NGMLR_CLR {
    publishDir "${params.outputDir}/fasttree_lofreq_NGMLR_CLR", mode: 'copy'
    input:
        file mfa from lofreqNGMLRCLROutputMfa3
    output:
        file "tree_file" into fasttree_lofreq_NGMLR_CLR
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_lofreq.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    """
}

process fasttree_breseq {
    publishDir "${params.outputDir}/fasttree_breseq", mode: 'copy'
    input:
        file mfa from breseqOutputMfa3
    output:
        file "tree_file" into fasttree_breseq
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_output.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    """
}

process fasttree_vcf_mfa_lofreq_bowtie {
    publishDir "${params.outputDir}/fasttree_vcf_mfa_lofreq_bowtie", mode: 'copy'
    input:
        file mfa from lofreqIlluminaBowtieOutputMfa3
        
    output:
        file "tree_file" into fasttree_lofreqIlluminaBowtie
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_lofreq.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    """
}

process fasttree_lofreq_Minimap {
    publishDir "${params.outputDir}/fasttree_lofreq_Minimap", mode: 'copy'
    input:
        file mfa from lofreqMinimapOutputMfa3
    output:
        file "tree_file" into fasttree_lofreqMinimap
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_lofreq.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file; 
    """
}

process fasttree_vcf_mfa_lofreq_CCS_Minimap {
    publishDir "${params.outputDir}/fasttree_vcf_mfa_lofreq_CCS_Minimap", mode: 'copy'
    input:
        file mfa from lofreqPacbioCCSMinimapOutputMfa3
    output:
        file "tree_file" into fasttree_lofreqPacbioCCSMinimap
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_lofreq.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    """
}

process fasttree_lofreq_Minimap_CLR {
    publishDir "${params.outputDir}/fasttree_lofreq_Minimap_CLR", mode: 'copy'
    input:
        file mfa from lofreqPacbioCLRMinimapOutputMfa3
    output:
        file "tree_file" into fasttree_lofreqPacbioCLRMinimap
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/_lofreq.vcf//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    """
}

process tree_compare_vcf {
    publishDir "${params.outputDir}/treeCompareVcf", mode: 'copy'
    input:
        // explicitly given input tree
        file originalTree from tree
        file "beastLofreqNGMLRCCS" from beast_lofreq_NGMLR_CCS1
        file "RaxmlLofreqNGMLRCCS" from Raxml_lofreq_NGMLR_CCS1
        file "fasttreeLofreqNGMLRCCS" from fasttree_lofreqNGMLRCCS
        file "beastLofreqNGMLRCLR" from Beast_lofreq_NGMLR_CLR1
        file "RaxmlLofreqNGMLRCLR" from Raxml_lofreq_NGMLR_CLR1
        file "fasttreeLofreqNGMLRCLR" from fasttree_lofreq_NGMLR_CLR
        file "beastBreseq" from beast_breseq1
        file "RaxmlBreseq" from Raxml_breseq1
        file "fasttreeBreseq" from fasttree_breseq
        file "beastLofreqBowtie" from beast_vcf_mfa_lofreq_bowtie1
        file "RaxmlLofreqBowtie" from Raxml_vcf_mfa_lofreq_bowtie1
        file "fasttreeLofreqBowtie" from fasttree_lofreqIlluminaBowtie
        file "beastLofreqMinimap" from beast_lofreq_Minimap1
        file "RaxmlLofreqMinimap" from Raxml_lofreq_Minimap1
        file "fasttreeLofreqMinimap" from fasttree_lofreqMinimap
        file "beastLofreqMinimapCCS" from beast_vcf_mfa_lofreq_CCS_Minimap1
        file "RaxmlLofreqMinimapCCS" from Raxml_vcf_mfa_lofreq_CCS_Minimap1
        file "fasttreeLofreqMinimapCCS" from fasttree_lofreqPacbioCCSMinimap
        file "beastLofreqMinimapCLR" from beast_lofreq_Minimap_CLR1
        file "RaxmlLofreqMinimapCLR" from Raxml_lofreq_Minimap_CLR1
        file "fasttreeLofreqMinimapCLR" from fasttree_lofreqPacbioCLRMinimap
    output:
        file "originalB.nexus" into tree_compare_python1
        file "summary.csv" into tree_compare_python2
    script:
        """
        #format_branch_length="%1.5f", so if want to decrease branch length, can only replace afterwards with branch length parameter
        python ${params.program}convertNewickToNexus.py $originalTree originalB.nexus; 
        #replace branch length here to skip the python bug
        sed -i "s/0.1/${params.branchLength}/g" originalB.nexus;
        python ${params.program}RBTreeCompare.py $originalTree summary.csv;
        """
}

process tree_compare_vcf_R {
    publishDir "${params.outputDir}/treeCompareVcfR", mode: 'copy'
    input:
        // explicitly given input tree, actually name doesn't matter
        file "originalTree" from tree
        file "originalB.nexus" from tree_compare_python1
        file "beastLofreqNGMLRCCS" from beast_lofreq_NGMLR_CCS1
        file "RaxmlLofreqNGMLRCCS" from Raxml_lofreq_NGMLR_CCS1
        file "fasttreeLofreqNGMLRCCS" from fasttree_lofreqNGMLRCCS
        file "beastLofreqNGMLRCLR" from Beast_lofreq_NGMLR_CLR1
        file "RaxmlLofreqNGMLRCLR" from Raxml_lofreq_NGMLR_CLR1
        file "fasttreeLofreqNGMLRCLR" from fasttree_lofreq_NGMLR_CLR
        file "beastBreseq" from beast_breseq1
        file "RaxmlBreseq" from Raxml_breseq1
        file "fasttreeBreseq" from fasttree_breseq
        file "beastLofreqBowtie" from beast_vcf_mfa_lofreq_bowtie1
        file "RaxmlLofreqBowtie" from Raxml_vcf_mfa_lofreq_bowtie1
        file "fasttreeLofreqBowtie" from fasttree_lofreqIlluminaBowtie
        file "beastLofreqMinimap" from beast_lofreq_Minimap1
        file "RaxmlLofreqMinimap" from Raxml_lofreq_Minimap1
        file "fasttreeLofreqMinimap" from fasttree_lofreqMinimap
        file "beastLofreqMinimapCCS" from beast_vcf_mfa_lofreq_CCS_Minimap1
        file "RaxmlLofreqMinimapCCS" from Raxml_vcf_mfa_lofreq_CCS_Minimap1
        file "fasttreeLofreqMinimapCCS" from fasttree_lofreqPacbioCCSMinimap
        file "beastLofreqMinimapCLR" from beast_lofreq_Minimap_CLR1
        file "RaxmlLofreqMinimapCLR" from Raxml_lofreq_Minimap_CLR1
        file "fasttreeLofreqMinimapCLR" from fasttree_lofreqPacbioCLRMinimap
    
    output:
        file "output" into treeCompareVcfR

    script:
        """
        mkdir output
        Rscript ${params.program}rScript.R originalTree RaxmlBreseq RaxmlLofreqBowtie RaxmlLofreqMinimap RaxmlLofreqMinimapCCS RaxmlLofreqMinimapCLR RaxmlLofreqNGMLRCCS RaxmlLofreqNGMLRCLR output/R_summary.csv output/RaxmlTreePlot.pdf;
        Rscript ${params.program}rScript.R originalTree fasttreeBreseq fasttreeLofreqBowtie fasttreeLofreqMinimap fasttreeLofreqMinimapCCS fasttreeLofreqMinimapCLR fasttreeLofreqNGMLRCCS fasttreeLofreqNGMLRCLR output/F_summary.csv output/FasttreeTreePlot.pdf;
        #Rscript ${params.program}rScript.R originalB.nexus beastBreseq beastLofreqBowtie beastLofreqMinimap beastLofreqMinimapCCS beastLofreqMinimapCLR beastLofreqNGMLRCCS beastLofreqNGMLRCLR output/B_summary.csv output/BeastTreePlot.pdf;
        """
}


process canu_pbSim{
    publishDir "${params.outputDir}/canu", mode: 'copy'
    tag "${replicateId}"
    maxForks 4
    input:
        set replicateId, file(reads) from read_ch_pbhifi.mix(pb_optional_hifi_ch_canu)
    output:
        set replicateId, file("${replicateId}/${replicateId}.contigs.fasta") into canuReceiver
    script:
    """
    paste - - - - < $reads | cut -f 1,2 | sed 's/^@/>/' | tr "\t" "\n" > reads.fasta; # fastq to fasta;
    canu -trim-assemble -p ${replicateId} -d ${replicateId} genomeSize=${params.genomeSize} -corrected -pacbio reads.fasta;
    """
}

process merge_canu_pacbio{
    input:
        file "*" from canuReceiver.collect()
    output:
        file "mergeCanuPacbio" into canuPacbioMergeReceiver1, canuPacbioMergeReceiver2

    script:
    """
    mkdir mergeCanuPacbio;
    scp *.contigs.fasta mergeCanuPacbio;
    """
}

process parsnp_pacbio {
    publishDir "${params.outputDir}/parsnp_canu_pacbio", mode: 'copy'
    input:
        file contigDir from long_align_ch_parsnp.mix(canuPacbioMergeReceiver2)
        file genome from oneRef
    output:
        file "canu_parsnp_out/parsnp.mfa" into canuParsnpReceiver1, canuParsnpReceiver2, canuParsnpReceiver3
    script:
        """
        #-r = <path>: (r)eference genome (set to ! to pick random one from genome dir)
        #-d = <path>: (d)irectory containing genomes/contigs
        #parsnp –p <threads> –d <directory of genomes> –r <ref genome>
        # error if the genome is in the workspace
        mv ${genome} reference.fasta;
        parsnp -r reference.fasta -d ${contigDir} -o canu_parsnp_out;
        cd canu_parsnp_out;
        harvesttools -i parsnp.ggr -M parsnp.mfa;
        """
}

process Raxml_parsnp_pacbio {
    publishDir "${params.outputDir}/raxml_parsnp_canu_pacbio", mode: 'copy'
    input:
        file mfa from canuParsnpReceiver1
    output:
        file "RAxML_bestTree._raxml" into raxmlParsnpPacbioReceiver1, raxmlParsnpPacbioReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fasta//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta.ref/Taxon0/g' RAxML_bestTree._raxml;
        """

}

process fasttree_parsnp_pacbio {
    publishDir "${params.outputDir}/fasttree_parsnp_pacbio", mode: 'copy'
    input:
        file mfa from canuParsnpReceiver3
    output:
        file "tree_file" into fasttree_parsnp_pacbio1, fasttree_parsnp_pacbio2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fasta//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta.ref/Taxon0/g' tree_file;
    """
}

process Beast_parsnp_pacbio {
    publishDir "${params.outputDir}/beast_parsnp_canu_pacbio", mode: 'copy'
    input:
        file mfa from canuParsnpReceiver2
    output:
        file "out.txt" into beastParsnpPacbioReceiver1, beastParsnpPacbioReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/.contigs.fasta//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta.ref/Taxon0/g' out.txt;
        """
}

process mauve_pacbio {
    publishDir "${params.outputDir}/mauve_canu_pacbio", mode: 'copy'
    input:
        file contigDir from long_align_ch_mauve.mix(canuPacbioMergeReceiver1)
        file ref from oneRef
    output:
        file "core_alignment.mfa" into (mauvePacbioReceiver1, mauvePacbioReceiver2, mauvePacbioReceiver3)
    script:
        """
        mv $ref reference.fasta;
        scp ${contigDir}/*.fasta ./;
        progressiveMauve --output=pacbioAlignment.xmfa --output-guide-tree=pacbioAlignment.tree --backbone-output=pacbioAlignment.backbone *.fasta;
        stripSubsetLCBs pacbioAlignment.xmfa pacbioAlignment.xmfa.bbcols core_alignment.xmfa 500;
        python ${params.program}concatenate.py core_alignment.xmfa core_alignment.mfa;
        """
}

process Raxml_mauve_pacbio {
    publishDir "${params.outputDir}/raxml_mauve_canu_pacbio", mode: 'copy'
    input:
        file mfa from mauvePacbioReceiver1
    output:
        file "RAxML_bestTree._raxml" into raxmlMauvePacbioReceiver1, raxmlMauvePacbioReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fasta//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta/Taxon0/g' RAxML_bestTree._raxml;
        """
}

process fasttree_mauve_pacbio {
    publishDir "${params.outputDir}/fasttree_mauve_canu_pacbio", mode: 'copy'
    input:
        file mfa from mauvePacbioReceiver3
    output:
        file "tree_file" into fasttree_mauve_pacbio1, fasttree_mauve_pacbio2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fasta//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta/Taxon0/g' tree_file;
    """
}

process Beast_mauve_pacbio {
    publishDir "${params.outputDir}/beast_mauve_canu_pacbio", mode: 'copy'
    input:
        file mfa from mauvePacbioReceiver2
    output:
        file "out.txt" into beastMauvePacbioReceiver1, beastMauvePacbioReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa pacbio.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template pacbio.nexus pacbio.xml;
        beast -seed 20 pacbio.xml;
        treeannotator -burninTrees 200 -heights mean pacbio.trees out.txt;
        sed -i 's/.contigs.fasta//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta/Taxon0/g' out.txt;
        """
}
//megahit
process megahit_illumina{
    tag "${replicateId}"
    publishDir "${params.outputDir}/art_megahit_assembly", mode: 'copy'
    maxForks 4

    input:
    set replicateId, file(read_1), file(read_2) from trimReceiver1

    output:
    set replicateId, "megahit_out/${replicateId}.contigs.fa" into megaReceiver

    script:
    """
    megahit -1 ${read_1} -2 ${read_2} --out-prefix ${replicateId};
    """
}

process merge_megahit{
    input:
    file "*" from megaReceiver.collect()

    output:
    file "mergeMegahit" into megahitMergeReceiver1, megahitMergeReceiver2

    script:
    """
    mkdir mergeMegahit;
    scp *contigs.fa mergeMegahit;
    """
}

process Parsnp_megahit{
    publishDir "${params.outputDir}/parsnp_megahit", mode: 'copy'
    input:
        file contigDir from short_align_ch_parsnp_megahit.mix(megahitMergeReceiver1)
        file ref from oneRef
    output:
        file "megahit_parsnp_out/parsnp.mfa" into megahitParsnpReceiver1, megahitParsnpReceiver2, megahitParsnpReceiver3
    script:
        """
        #-r = <path>: (r)eference genome (set to ! to pick random one from genome dir)
        #-d = <path>: (d)irectory containing genomes/contigs
        #parsnp –p <threads> –d <directory of genomes> –r <ref genome>
        mv $ref reference.fasta;
        parsnp -r reference.fasta -d ${contigDir} -o megahit_parsnp_out;
        cd megahit_parsnp_out;
        harvesttools -i parsnp.ggr -M parsnp.mfa;
        """
}

process RAxML_parsnp_megahit{
    publishDir "${params.outputDir}/raxml_parsnp_megahit", mode: 'copy'
    input:
        file mfa from megahitParsnpReceiver1
    output:
        file "RAxML_bestTree._raxml" into raxmlMegahitParsnpReceiver1, raxmlMegahitParsnpReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fa//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta.ref/Taxon0/g' RAxML_bestTree._raxml;
        """
}

process fasttree_parsnp_megahit {
    publishDir "${params.outputDir}/fasttree_parsnp_megahit", mode: 'copy'
    input:
        file mfa from megahitParsnpReceiver3
    output:
        file "tree_file" into fasttree_parsnp_megahit1, fasttree_parsnp_megahit2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fa//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta.ref/Taxon0/g' tree_file;
    """
}

process Beast_parsnp_megahit{
    publishDir "${params.outputDir}/beast_parsnp_megahit", mode: 'copy'
    input:
        file mfa from megahitParsnpReceiver2
    output:
        file "out.txt" into beastParsnpMegahitReceiver1, beastParsnpMegahitReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa illumina.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template illumina.nexus illumina.xml;
        beast -seed 20 illumina.xml;
        treeannotator -burninTrees 200 -heights mean illumina.trees out.txt;
        sed -i 's/.contigs.fa//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta.ref/Taxon0/g' out.txt;
        """
}

//We ran SPAdes with default parameters using 16 threads on a server
//8.1 Gb
process spades {
        tag "${replicateId}"
        publishDir "${params.outputDir}/art_spades_assembly", mode: 'copy'
        maxForks 4
    input:
        set replicateId, file(read_1), file(read_2) from trimReceiver2
    output:
        set replicateId, "spades_out/${replicateId}.contigs.fasta" into spadesReceiver
    script:
        """
        /phylo_programs/SPAdes-3.13.1-Linux/bin/spades.py --pe1-1 ${read_1} --pe1-2 ${read_2} --only-assembler -o spades_out;
        cd spades_out;
        mv contigs.fasta ${replicateId}.contigs.fasta;
        """
}

// merge spades channel into one channel
process merge_spades{
    input:
    file "*" from spadesReceiver.collect()

    output:
    file "mergeSpades" into spadeMergeReceiver1, spadeMergeReceiver2

    script:
    """
    mkdir mergeSpades;
    scp *.contigs.fasta mergeSpades;
    """
}

process Mauve_spades{
    publishDir "${params.outputDir}/mauve_spades", mode: 'copy'
    input:
        file contigDir from short_align_ch_mauve_spades.mix(spadeMergeReceiver1)
        file ref from oneRef
    output:
        file "core_alignment.mfa" into (mauveSpadesReceiver1, mauveSpadesReceiver2, mauveSpadesReceiver3)
    script:
        """
        mv $ref reference.fasta;
        scp ${contigDir}/*.fasta ./;
        progressiveMauve --output=illuminaAlignment.xmfa --output-guide-tree=illuminaAlignment.tree --backbone-output=illuminaAlignment.backbone *.fasta;
        stripSubsetLCBs illuminaAlignment.xmfa illuminaAlignment.xmfa.bbcols core_alignment.xmfa 500;
        python ${params.program}concatenate.py core_alignment.xmfa core_alignment.mfa;
        """
}

process RAxML_mauve_spades{
    publishDir "${params.outputDir}/raxml_mauve_spades", mode: 'copy'
    input:
        file mfa from mauveSpadesReceiver2
    output:
        file "RAxML_bestTree._raxml" into raxmlSpadesMauveReceiver1, raxmlSpadesMauveReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fasta//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta/Taxon0/g' RAxML_bestTree._raxml;
        """
}

process fasttree_mauve_spades {
    publishDir "${params.outputDir}/fasttree_mauve_spades", mode: 'copy'
    input:
        file mfa from mauveSpadesReceiver3
    output:
        file "tree_file" into fasttree_mauve_spades1, fasttree_mauve_spades2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fasta//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta/Taxon0/g' tree_file;
    """
}

process Beast_mauve_spades{
    publishDir "${params.outputDir}/beast_mauve_spades", mode: 'copy'
    input:
        file mfa from mauveSpadesReceiver1
    output:
        file "out.txt" into beastMauveSpadesReceiver1, beastMauveSpadesReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa illumina.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template illumina.nexus illumina.xml;
        beast -seed 20 illumina.xml;
        treeannotator -burninTrees 200 -heights mean illumina.trees out.txt;
        sed -i 's/.contigs.fasta//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta/Taxon0/g' out.txt;
        """
}

// all spades contigs files into one parsnp process
process Parsnp_spades{

    publishDir "${params.outputDir}/parsnp_spades", mode: 'copy'
    input:
        file contigDir from short_align_ch_parsnp_spades.mix(spadeMergeReceiver2)
        file ref from oneRef
    output:
        file "spades_parsnp_out/parsnp.mfa" into spadesParsnpReceiver1, spadesParsnpReceiver2, spadesParsnpReceiver3
    script:
        """
        #-r = <path>: (r)eference genome (set to ! to pick random one from genome dir)
        #-d = <path>: (d)irectory containing genomes/contigs
        #parsnp –p <threads> –d <directory of genomes> –r <ref genome>
        mv $ref reference.fasta;
        parsnp -r reference.fasta -d ${contigDir} -o spades_parsnp_out;
        cd spades_parsnp_out;
        harvesttools -i parsnp.ggr -M parsnp.mfa;
        """
}

//RAXML parsnp spades
process RAxML_parsnp_spades{
    publishDir "${params.outputDir}/raxml_parsnp_spades", mode: 'copy'
    input:
        file mfa from spadesParsnpReceiver1
    output:
        file "RAxML_bestTree._raxml" into raxmlSpadesParsnpReceiver1, raxmlSpadesParsnpReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fasta//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta.ref/Taxon0/g' RAxML_bestTree._raxml;
        """
}

process fasttree_parsnp_spades {
    publishDir "${params.outputDir}/fasttree_parsnp_spades", mode: 'copy'
    input:
        file mfa from spadesParsnpReceiver3
    output:
        file "tree_file" into fasttree_parsnp_spades1, fasttree_parsnp_spades2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fasta//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta.ref/Taxon0/g' tree_file;
    """
}

process Beast_parsnp_spades{
    publishDir "${params.outputDir}/beast_parsnp_spades", mode: 'copy'
    input:
        file mfa from spadesParsnpReceiver2
    output:
        file "out.txt" into beastParsnpSpadesReceiver1, beastParsnpSpadesReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa illumina.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template illumina.nexus illumina.xml;
        beast -seed 20 illumina.xml;
        treeannotator -burninTrees 200 -heights mean illumina.trees out.txt;
        sed -i 's/.contigs.fasta//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta.ref/Taxon0/g' out.txt;
        """
}

//spades_hybrid
process spades_hybrid{
tag "${replicateId}"
        publishDir "${params.outputDir}/art_padbio_spades_hybrid_assembly", mode: 'copy'
        maxForks 4
    input:
        set replicateId, file(read_1), file(read_2) from trimReceiver3
        set replicateId, file(pbRead) from pb_CLR_ch_spades_hybrid
    output:
        set replicateId, "spades_out/${replicateId}.contigs.fasta" into spadesHybridReceiver
    script:
        """
        #File with PacBio CLR reads. For PacBio CCS reads use -s option.
        /phylo_programs/SPAdes-3.13.1-Linux/bin/spades.py --pe1-1 ${read_1} --pe1-2 ${read_2} --only-assembler --pacbio $pbRead -o spades_out;
        cd spades_out;
        mv contigs.fasta ${replicateId}.contigs.fasta;
        """
}

// merge spadesHybrid channel into one channel
process merge_spadesHybrid{
    input:
    file "*" from spadesHybridReceiver.collect()

    output:
    file "mergeSpades" into spadeHybridMergeReceiver1, spadeHybridMergeReceiver2

    script:
    """
    mkdir mergeSpades;
    scp *.contigs.fasta mergeSpades;
    """
}
//Spades Hybrid
process Mauve_spadesHybrid{
    publishDir "${params.outputDir}/mauve_spadesHybrid", mode: 'copy'
    input:
        file contigDir from short_align_ch_mauve_spades_hybrid.mix(spadeHybridMergeReceiver1)
        file ref from oneRef
    output:
        file "core_alignment.mfa" into (mauveSpadesHybridReceiver1, mauveSpadesHybridReceiver2, mauveSpadesHybridReceiver3)
    script:
        """
        mv $ref reference.fasta;
        scp ${contigDir}/*.fasta ./;
        progressiveMauve --output=illuminaAlignment.xmfa --output-guide-tree=illuminaAlignment.tree --backbone-output=illuminaAlignment.backbone *.fasta;
        stripSubsetLCBs illuminaAlignment.xmfa illuminaAlignment.xmfa.bbcols core_alignment.xmfa 500;
        python ${params.program}concatenate.py core_alignment.xmfa core_alignment.mfa;
        """
}

process RAxML_mauve_spadesHybrid{
    publishDir "${params.outputDir}/raxml_mauve_spadesHybrid", mode: 'copy'
    input:
        file mfa from mauveSpadesHybridReceiver2
    output:
        file "RAxML_bestTree._raxml" into raxmlSpadesHybridMauveReceiver1, raxmlSpadesHybridMauveReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fasta//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta/Taxon0/g' RAxML_bestTree._raxml;
        """
}

process fasttree_mauve_spadesHybrid {
    publishDir "${params.outputDir}/fasttree_mauve_spadesHybrid", mode: 'copy'
    input:
        file mfa from mauveSpadesHybridReceiver3
    output:
        file "tree_file" into fasttree_mauve_spadesHybrid1, fasttree_mauve_spadesHybrid2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fasta//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta/Taxon0/g' tree_file;
    """
}

process Beast_mauve_spadesHybrid{
    publishDir "${params.outputDir}/beast_mauve_spadesHybrid", mode: 'copy'
    input:
        file mfa from mauveSpadesHybridReceiver1
    output:
        file "out.txt" into beastMauveSpadesHybridReceiver1, beastMauveSpadesHybridReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa illumina.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template illumina.nexus illumina.xml;
        beast -seed 20 illumina.xml;
        treeannotator -burninTrees 200 -heights mean illumina.trees out.txt;
        sed -i 's/.contigs.fasta//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta/Taxon0/g' out.txt;
        """
}

// all spades contigs files into one parsnp process
process Parsnp_spadesHybrid{

    publishDir "${params.outputDir}/parsnp_spadesHybrid", mode: 'copy'
    input:
        file contigDir from short_align_ch_parsnp_hybrid.mix(spadeHybridMergeReceiver2)
        file ref from oneRef
    output:
        file "spades_parsnp_out/parsnp.mfa" into spadesHybridParsnpReceiver1, spadesHybridParsnpReceiver2, spadesHybridParsnpReceiver3
    script:
        """
        #-r = <path>: (r)eference genome (set to ! to pick random one from genome dir)
        #-d = <path>: (d)irectory containing genomes/contigs
        #parsnp –p <threads> –d <directory of genomes> –r <ref genome>
        mv $ref reference.fasta;
        parsnp -r reference.fasta -d ${contigDir} -o spades_parsnp_out;
        cd spades_parsnp_out;
        harvesttools -i parsnp.ggr -M parsnp.mfa;
        """
}

//RAXML parsnp spades
process RAxML_parsnp_spadesHybrid{
    publishDir "${params.outputDir}/raxml_parsnp_spadesHybrid", mode: 'copy'
    input:
        file mfa from spadesHybridParsnpReceiver1
    output:
        file "RAxML_bestTree._raxml" into raxmlSpadesHybridParsnpReceiver1, raxmlSpadesHybridParsnpReceiver2
    script:
        """
        raxmlHPC-SSE3 -s $mfa -n _raxml -m GTRGAMMA -p 20;
        sed -i 's/.contigs.fasta//g' RAxML_bestTree._raxml;
        sed -i 's/genome_/Taxon/g' RAxML_bestTree._raxml;
        sed -i 's/reference.fasta.ref/Taxon0/g' RAxML_bestTree._raxml;
        """
}

process fasttree_parsnp_spadesHybrid {
    publishDir "${params.outputDir}/fasttree_parsnp_spadesHybrid", mode: 'copy'
    input:
        file mfa from spadesHybridParsnpReceiver3
    output:
        file "tree_file" into fasttree_parsnp_spadesHybrid1, fasttree_parsnp_spadesHybrid2
    script:
    """
    #If you do not specify -gtr, then FastTree will use the Jukes-Cantor + CAT model instead.
    #Use the -gamma option (about 5% slower) if you want to rescale the branch lengths and compute a Gamma20-based likelihood. Gamma likelihoods are more comparable across runs.
    #These also allow for statistical comparisons of the likelihood of different topologies if you use the -log logfile option (see details).
    FastTree -nt $mfa > tree_file;
    sed -i 's/.contigs.fasta//g' tree_file;
    sed -i 's/genome_/Taxon/g' tree_file;
    sed -i 's/reference.fasta.ref/Taxon0/g' tree_file;
    """
}

process Beast_parsnp_spadesHybrid{
    publishDir "${params.outputDir}/beast_parsnp_spadesHybrid", mode: 'copy'
    input:
        file mfa from spadesHybridParsnpReceiver2
    output:
        file "out.txt" into beastParsnpSpadesHybridReceiver1, beastParsnpSpadesHybridReceiver2
    script:
        """
        python ${params.program}convertFastaToNexus.py $mfa illumina.nexus;
        scp /phylo_programs/BEASTGen_v1.0.2/templates/beast_example.template ./;
        beastgen -D "chain_length=10000000,log_every=5000" beast_example.template illumina.nexus illumina.xml;
        beast -seed 20 illumina.xml;
        treeannotator -burninTrees 200 -heights mean illumina.trees out.txt;
        sed -i 's/.contigs.fasta//g' out.txt;
        sed -i 's/genome_/Taxon/g' out.txt;
        sed -i 's/reference.fasta.ref/Taxon0/g' out.txt;
        """
}

process tree_compare_aa {
    publishDir "${params.outputDir}/treeCompareAa", mode: 'copy'
    input:
        // explicitly given input treeR
        file originalTree from treeR //1
        file "raxmlSpadesHybridParsnp" from raxmlSpadesHybridParsnpReceiver1//2
        file "raxmlSpadesHybridMauve" from raxmlSpadesHybridMauveReceiver1//3
        file "raxmlSpadesParsnp" from raxmlSpadesParsnpReceiver1//4
        file "raxmlSpadesMauve" from raxmlSpadesMauveReceiver1//5
        file "raxmlMegahitParsnp" from raxmlMegahitParsnpReceiver1//6
        file "raxmlMauvePacbio" from raxmlMauvePacbioReceiver1//7
        file "raxmlParsnpPacbio" from raxmlParsnpPacbioReceiver1//8

        file "fasttreeSpadesHybridParsnp" from fasttree_parsnp_spadesHybrid1
        file "fasttreeSpadesHybridMauve" from fasttree_mauve_spadesHybrid1
        file "fasttreeSpadesParsnp" from fasttree_parsnp_spades1
        file "fasttreeSpadesMauve" from fasttree_mauve_spades1
        file "fasttreeMegahitParsnp" from fasttree_parsnp_megahit1
        file "fasttreeMauvePacbio" from fasttree_mauve_pacbio1
        file "fasttreeParsnpPacbio" from fasttree_parsnp_pacbio1

        file "beastSpadesHybridParsnp" from beastParsnpSpadesHybridReceiver1
        file "beastSpadesHybridMauve" from beastMauveSpadesHybridReceiver1
        file "beastSpadesParsnp" from beastParsnpSpadesReceiver1
        file "beastSpadesMauve" from beastMauveSpadesReceiver1
        file "beastMegahitParsnp" from beastParsnpMegahitReceiver1
        file "beastMauvePacbio" from beastMauvePacbioReceiver1
        file "beastParsnpPacbio" from beastParsnpPacbioReceiver1
        
    output:
        file "originalB.nexus" into tree_compare_python_aa1
        file "summary.csv" into tree_compare_python_aa2
    script:
        """
        #format_branch_length="%1.5f", so if want to decrease branch length, can only replace afterwards with branch length parameter
        python ${params.program}convertNewickToNexus.py $originalTree originalB.nexus; 
        #replace branch length here to skip the python bug, respect the real near zero branch length (substitution per site) and skip python code 
        sed -i "s/0.1/${params.branchLength}/g" originalB.nexus;
        python ${params.program}RBTreeCompareAA.py $originalTree summary.csv;
        """
}

process tree_compare_aa_R {
    publishDir "${params.outputDir}/treeCompareAaR", mode: 'copy'
    input:
        // explicitly given input tree, actually name doesn't matter
        file "originalTree" from treeR //1
        file "originalB.nexus" from tree_compare_python_aa1
        file "raxmlSpadesHybridParsnp" from raxmlSpadesHybridParsnpReceiver2//2
        file "raxmlSpadesHybridMauve" from raxmlSpadesHybridMauveReceiver2//3
        file "raxmlSpadesParsnp" from raxmlSpadesParsnpReceiver2//4
        file "raxmlSpadesMauve" from raxmlSpadesMauveReceiver2//5
        file "raxmlMegahitParsnp" from raxmlMegahitParsnpReceiver2//6
        file "raxmlMauvePacbio" from raxmlMauvePacbioReceiver2//7
        file "raxmlParsnpPacbio" from raxmlParsnpPacbioReceiver2//8

        file "fasttreeSpadesHybridParsnp" from fasttree_parsnp_spadesHybrid2
        file "fasttreeSpadesHybridMauve" from fasttree_mauve_spadesHybrid2
        file "fasttreeSpadesParsnp" from fasttree_parsnp_spades2
        file "fasttreeSpadesMauve" from fasttree_mauve_spades2
        file "fasttreeMegahitParsnp" from fasttree_parsnp_megahit2
        file "fasttreeMauvePacbio" from fasttree_mauve_pacbio2
        file "fasttreeParsnpPacbio" from fasttree_parsnp_pacbio2

        file "beastSpadesHybridParsnp" from beastParsnpSpadesHybridReceiver2
        file "beastSpadesHybridMauve" from beastMauveSpadesHybridReceiver2
        file "beastSpadesParsnp" from beastParsnpSpadesReceiver2
        file "beastSpadesMauve" from beastMauveSpadesReceiver2
        file "beastMegahitParsnp" from beastParsnpMegahitReceiver2
        file "beastMauvePacbio" from beastMauvePacbioReceiver2
        file "beastParsnpPacbio" from beastParsnpPacbioReceiver2
    
    output:
        file "output" into treeCompareAaR

    script:
        """
        mkdir output
        Rscript ${params.program}rScript.R originalTree raxmlSpadesHybridParsnp raxmlSpadesHybridMauve raxmlSpadesParsnp raxmlSpadesMauve raxmlMegahitParsnp raxmlMauvePacbio raxmlParsnpPacbio output/R_summary.csv output/RaxmlTreePlot.pdf;
        #Rscript ${params.program}rScript.R originalTree fasttreeSpadesHybridParsnp fasttreeSpadesHybridMauve fasttreeSpadesParsnp fasttreeSpadesMauve fasttreeMegahitParsnp fasttreeMauvePacbio fasttreeParsnpPacbio output/F_summary.csv output/FasttreeTreePlot.pdf;
        #Rscript ${params.program}rScript.R originalB.nexus beastSpadesHybridParsnp beastSpadesHybridMauve beastSpadesParsnp beastSpadesMauve beastMegahitParsnp beastMauvePacbio beastParsnpPacbio output/B_summary.csv output/BeastTreePlot.pdf;
        """
    }